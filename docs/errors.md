This document is a copy of an [error message index provided by TI][ti-index]. It lists the error messages that can be
shown by a calculator and describes the possible causes. Not all of these errors can be emitted while running a TI-BASIC
program, though most can.

[ti-index]: https://education.ti.com/html/webhelp/EG_TI84PlusCE/UK/Subsystems/eg_ti84plusce_ref/content/m_errors/em_errormessages.HTML

* `ARCHIVED`: You have attempted to use, edit, or delete an archived variable. For example, the expression dim(L1)
  produces an error if L1 is archived.

* `ARCHIVE FULL` You have attempted to archive a variable and there is not enough space in archive to receive it.

* `ARGUMENT`: A function or instruction does not have the correct number of arguments.

  The arguments are shown in italics. The arguments in brackets are optional and you need not type them. You must also
  be sure to separate multiple arguments with a comma (`,`). For example, `stdDev(list[,freqlist])` might be entered as
  `stdDev(L1)` or `stdDev(L1,L2)` since the frequency list or freqlist is optional.

* `BAD ADDRESS`: You have attempted to send or receive an application and an error (e.g. electrical interference) has
  occurred in the transmission.

* `BAD GUESS`: In a CALC operation, you specified a Guess that is not between Left Bound and Right Bound. For the solve(
  function or the equation solver, you specified a guess that is not between lower and upper. Your guess and several
  points around it are undefined. Examine a graph of the function. If the equation has a solution, change the bounds
  and/or the initial guess.

* `BOUND`: In a CALC operation or with Select(, you defined Left Bound > Right Bound. In fMin(, fMax(, solve(, or the
  equation solver, you entered lower > upper.

* `BREAK`: You pressed the `ON` key to break execution of a programm, to halt a DRAW instruction, or to stop evaluation
  of an expression.

* `DATA TYPE`: You entered a value or variable that is the wrong data type.
    * For a function (including implied multiplication) or an instruction, you entered an argument that is an invalid
      data type, such as a complex number where a real number is required.
    * In an editor, you entered a type that is not allowed, such as a matrix entered as an element in the stat list
      editor.
    * You attempted to store an incorrect data type, such as a matrix, to a list.
    * You attempted to enter complex numbers into the n/d MathPrint™ template.

* `DIMENSION MISMATCH`: Your calculator displays the ERR:DIMENSION MISMATCH error if you are trying to perform an
  operation that references one or more lists or matrices whose dimensions do not match. For example, multiplying L1*L2,
  where L1={1,2,3,4,5} and L2={1,2} produces an ERR:DIMENSION MISMATCH error because the number of elements in L1 and L2
  do not match. You may need to turn Plots Off to continue.

* `DIVIDE BY 0`: You attempted to divide by zero. This error is not returned during graphing. The TI‑84 Plus CE allows
  for undefined values on a graph. This also occurs if a linear regression is attempted with a vertical line.

* `DOMAIN`:
    * You specified an argument to a function or instruction outside the valid range. The TI‑84 Plus CE allows for
      undefined values on a graph.
    * You attempted a logarithmic or power regression with a LX or an exponential or power regression with a LY.
    * You attempted to compute GPrn( or GInt( with pmt2 < pmt1.

* `DUPLICATE`: You attempted to create a duplicate group name.

* `Duplicate Name`: A variable you attempted to transmit cannot be transmitted because a variable with that name already
  exists in the receiving unit.

* `EXPIRED`: You have attempted to run an application with a limited trial period which has expired.

* `Error in Xmit`: The calculator was unable to transmit an item. Check to see that the cable is firmly connected to
  both units and that the receiving unit is in receive mode.
    * You pressed `ON` to break during transmission.
    * Setup RECEIVE first and then SEND, when sending files (8) between graphing calculators.

* `ID NOT FOUND`: This error occurs when the SendID command is executed but the proper graphing calculator ID cannot be
  found.

* `ILLEGAL NEST`: You attempted to use an invalid function in an argument to a function, such as seq( within expression
  for seq(.

* `INCREMENT`:
    * The increment, step, in seq( is 0 or has the wrong sign.
    * The increment in a For( loop is 0.

* `INVALID`:
    * You attempted to reference a variable or use a function where it is not valid. For example, Yn cannot reference Y,
      Xmin, @X, or TblStart.
    * In Seq mode, you attempted to graph a phase plot without defining both equations of the phase plot.
    * In Seq mode, you attempted to graph a recursive sequence without having input the correct number of initial
      conditions.
    * In Seq mode, you attempted to reference terms other than (nN1) or (nN2).
    * You attempted to designate a graph style that is invalid within the current graph mode.
    * You attempted to use Select( without having selected (turned on) at least one xyLine or scatter plot.

* `INVALID DIMENSION`: The ERR:INVALID DIMENSION error message may occur if you are trying to graph a function that does
  not involve the stat plot features. The error can be corrected by turning off the stat plots. To turn the stat plots
  off, press y , and then select 4:PlotsOff.
    * You specified a list dimension as something other than an integer between 1 and 999.
    * You specified a matrix dimension as something other than an integer between 1 and 99.
    * You attempted to invert a matrix that is not square.

* `ITERATIONS`:
    * The solve( function or the equation solver has exceeded the maximum number of permitted iterations. Examine a
      graph of the function. If the equation has a solution, change the bounds, or the initial guess, or both.
    * irr( has exceeded the maximum number of permitted iterations.
    * When computing æ, the maximum number of iterations was exceeded.

* `LABEL`: The label in the Goto instruction is not defined with a Lbl instruction in the program.

* `LINK L1 (or any other file) to Restore`: The calculator is in test mode. To restore full functionality, use linking
  software to download a file to your calculator from a computer, or transfer any file to your calculator from another
  calculator.

* `MEMORY`: Memory is insufficient to perform the instruction or function. You must delete items from memory before
  executing the instruction or function.
    * Recursive problems return this error; for example, graphing the equation Y1=Y1.
    * Branching out of an If/Then, For(, While, or Repeat loop with a Goto also can return this error because the End
      statement that terminates the loop is never reached.
    * Attempting to create a matrix with larger than 400 cells.

* `MemoryFull`: You are unable to transmit an item because the receiving unit’s available memory is insufficient. You
  may skip the item or exit receive mode.

  During a memory backup, the receiving unit’s available memory is insufficient to receive all items in the sending
  unit’s memory. A message indicates the number of bytes the sending unit must delete to do the memory backup. Delete
  items and try again.

* `MODE`: You attempted to store to a window variable in another graphing mode or to perform an instruction while in the
  wrong mode; for example, DrawInv in a graphing mode other than Func.

* `NO SIGN CHANGE`:
    * The solve( function or the equation solver did not detect a sign change.
    * You attempted to compute æ when FV, (ÚPMT), and PV are all  0, or when FV, (ÚPMT), and PV are all _ 0.
    * You attempted to compute irr( when neither CFList nor CFO is > 0, or when neither CFList nor CFO is < 0.

* `NONREAL ANSWERS`: In Real mode, the result of a calculation yielded a complex result. . The TI‑84 Plus CE allows for
  undefined values on a graph.

* `OVERFLOW`: You attempted to enter, or you have calculated, a number that is beyond the range of the graphing
  calculator. The TI‑84 Plus CE allows for undefined values on a graph.

* `RESERVED`: You attempted to use a system variable inappropriately.

* `SINGULAR MATRIX`:
    * A singular matrix (determinant = 0) is not valid as the argument for L1.
    * The SinReg instruction or a polynomial regression generated a singular matrix (determinant = 0) because the
      algorithm could not find a solution, or a solution does not exist.

* `SINGULARITY`: expression in the solve( function or the equation solver contains a singularity (a point at which the
  function is not defined). Examine a graph of the function. If the equation has a solution, change the bounds or the
  initial guess or both.

* `STAT`: You attempted a stat calculation with lists that are not appropriate.
    * Statistical analyses must have at least two data points.
    * Med‑Med must have at least three points in each partition.
    * When you use a frequency list, its elements must be > 0.
    * (Xmax N Xmin) à Xscl must be between 0 and 131 for a histogram.

* `STAT PLOT`: You attempted to display a graph when a stat plot that uses an undefined list is turned on.

* `SYNTAX`: The command contains a syntax error. Look for misplaced functions, arguments, parentheses, or commas.

  For example, `stdDev(list[,freqlist])` is a function. The arguments are shown in italics. The arguments in brackets
  are optional and you need not type them. You must also be sure to separate multiple arguments with a comma (`,`). For
  example `stdDev(list[,freqlist])` might be entered as `stdDev(L1)` or `stdDev(L1,L2)` since the frequency list or
  `freqlist` is optional.

* `TOLERANCE NOT MET`: You requested a tolerance to which the algorithm cannot return an accurate result.

* `UNDEFINED`: You referenced a variable that is not currently defined. For example, you referenced a stat variable when
  there is no current calculation because a list has been edited, or you referenced a variable when the variable is not
  valid for the current calculation, such as a after Med‑Med.

* `VALIDATION`: Electrical interference caused a link to fail or this graphing calculator is not authorised to run the
  application.

* `VARIABLE`: You have tried to archive a variable that cannot be archived or you have tried to unarchive an application
  or group. Examples of variables that cannot be archived include:
    * Real numbers LRESID, R, T, X, Y, Theta
    * Statistic variables under Vars, STATISTICS menu
    * Yvars
    * the AppIdList.

* `VERSION`: You have attempted to receive an incompatible variable version from another graphing calculator.

  A program may contain commands not supported in the OS version on your graphing calculator. Always use the latest OS.
  TI-84 Plus CE and TI-84 Plus share programs but a version error will be given if any new TI-84 Plus CE programs may
  need to be adjusted for the high resolution graph area.

* `WINDOW RANGE`: A problem exists with the window variables.
    * You defined Xmax < Xmin or Ymax < Ymin.
    * You defined qmax < qmin and qstep > 0 (or vice versa).
    * You attempted to define Tstep=0.
    * You defined Tmax < Tmin and Tstep > 0 (or vice versa).
    * Window variables are too small or too large to graph correctly. You may have attempted to zoom to a point that
      exceeds the system's numerical range.

* `ZOOM`:
    * A point or a line, instead of a box, is defined in ZBox.
    * A ZOOM operation returned a maths error.
