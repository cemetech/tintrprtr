## ti83pkeys

This is a font face published by TI at
https://education.ti.com/en/software/details/en/3D36C30FA755411D93173BC370557992/83p
which is meant to replicate the legends printed on the calculator keys.
The glyphs are all in the Unicode private use space, each one representing
a whole key. Several glyphs that don't correspond to marking on the keyboard
(such as cursors or other displayed symbols) are also present.

## ti8x-large

As documented in the accompanying readme, this font is meant to replicate
the monochrome calculators' large font. However I believe it is not a pure
replica because it contains characters that the calculator cannot display
(particularly assorted CJK and Cyrillic characters).

## TICELarge

This font is the large (homescreen) font for the color-screen CE calculators.
It was created by Jacobly and published at https://jacobly.com/tifonts/
