//! Calculator character set glyphs.
//!
//! The functions in this module return bitmap glyphs, which are MSb-aligned in a row-major format
//! and never more than 8 pixels wide.

/// The width of glyphs in the large font, in pixels.
pub const LARGE_WIDTH: usize = 6;

/// The height of glyphs in the large font, in pixels.
///
/// Glyphs never exceed this height, but they conventionally have one row of padding at the top
/// which is not included in the glyph itself.
pub const LARGE_HEIGHT: usize = 7;

/// Get the glyph for a character in the large font.
pub fn get_large_glyph(c: u8) -> [u8; LARGE_HEIGHT] {
    let mut out = [0u8; LARGE_HEIGHT];
    let bitmap_index = c as usize * LARGE_HEIGHT;
    out.copy_from_slice(&LARGE_FONT[bitmap_index..bitmap_index + LARGE_HEIGHT]);
    out
}

static LARGE_FONT: &[u8] = include_bytes!("lgfont.bin");

/// The height of glyphs in the small font, in pixels.
pub const SMALL_HEIGHT: usize = 6;

/// Get the glyph for a character in the small font.
///
/// The small font is variable-width, so in addition to the glyph data itself the width of the glyph
/// in pixels is also returned.
pub fn get_small_glyph(c: u8) -> ([u8; SMALL_HEIGHT], u8) {
    let mut out = [0u8; SMALL_HEIGHT];
    let bitmap_index = c as usize * SMALL_HEIGHT;

    out.copy_from_slice(&SMALL_FONT[bitmap_index..bitmap_index + SMALL_HEIGHT]);
    (out, SMALL_FONT_WIDTHS[c as usize])
}

static SMALL_FONT: &[u8] = include_bytes!("smlfont.bin");
static SMALL_FONT_WIDTHS: &[u8] = include_bytes!("smlfont_widths.bin");
