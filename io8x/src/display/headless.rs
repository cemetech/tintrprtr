use super::{Display, PixelBuffer};
use std::fmt::Formatter;

#[derive(Eq, PartialEq, Clone)]
pub struct Headless(PixelBuffer);

impl Default for Headless {
    fn default() -> Self {
        Headless(PixelBuffer::default())
    }
}

impl Headless {
    pub fn new() -> Self {
        Default::default()
    }
}

impl Display for Headless {
    type BufRef<'a> = &'a PixelBuffer;
    type BufRefMut<'a> = &'a mut PixelBuffer;

    fn buf(&self) -> &PixelBuffer {
        &self.0
    }

    fn buf_mut(&mut self) -> &mut PixelBuffer {
        &mut self.0
    }
}

impl std::fmt::Debug for Headless {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}
