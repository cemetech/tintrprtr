use std::mem::MaybeUninit;
use std::ops::{Deref, DerefMut};

pub use headless::Headless;

pub mod headless;
pub mod proxy;
#[cfg(feature = "windowed")]
pub mod windowed;

pub const WIDTH: usize = 96;
pub const HEIGHT: usize = 64;

/// The format used for display pixel buffers.
///
/// The pixel buffer simply represents one pixel per byte in a row-major order starting from the
/// top left (so the first byte of the pixel buffer is the top left corner of the display, and the
/// last byte is the bottom right corner).
///
/// Each byte represents a monochrome pixel that displays only black or white. A zero value
/// represents a white pixel, and any nonzero value is a black pixel.
#[derive(Eq, PartialEq, Clone)]
pub struct PixelBuffer([u8; HEIGHT * WIDTH]);

impl Deref for PixelBuffer {
    type Target = [u8; HEIGHT * WIDTH];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for PixelBuffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Default for PixelBuffer {
    fn default() -> Self {
        PixelBuffer([0u8; 6144])
    }
}

impl PixelBuffer {
    /// Get a mutable view of the pixel buffer as an array of rows.
    ///
    /// This points to the same data as the raw pixel buffer, but it is
    /// sometimes more convenient to have individual rows presented separately.
    ///
    /// ```
    /// # use io8x::display::{PixelBuffer, WIDTH};
    /// let mut b: PixelBuffer = Default::default();
    ///
    /// b.as_mut_rows()[12][12] = 0xFF;
    /// assert_eq!(b[WIDTH * 12 + 12], 0xFF);
    /// ```
    pub fn as_mut_rows(&mut self) -> [&mut [u8; WIDTH]; HEIGHT] {
        let mut flat = self.as_mut_slice();
        // We know statically that the flat buffer can be treated as a collection of
        // rows; this use of MaybeUninit works like split_array_mut and friends, but
        // doesn't depend on an experimental API.
        let mut out: MaybeUninit<[&mut [u8; WIDTH]; HEIGHT]> = MaybeUninit::uninit();
        let outp = out.as_mut_ptr();

        for i in 0..HEIGHT {
            let (row, tail) = flat.split_at_mut(WIDTH);
            flat = tail;
            let row: &mut [u8; WIDTH] = row.try_into().unwrap();
            unsafe {
                std::ptr::addr_of_mut!((*outp)[i]).write(row);
            }
        }
        debug_assert_eq!(flat.len(), 0);

        unsafe { out.assume_init() }
    }

    /// The same as [`as_mut_rows`](Self::as_mut_rows), but returns immutable references.
    pub fn as_rows(&self) -> [&[u8; WIDTH]; HEIGHT] {
        let flat = self.as_slice();
        let mut out: MaybeUninit<[&[u8; WIDTH]; HEIGHT]> = MaybeUninit::uninit();
        let outp = out.as_mut_ptr();

        for i in 0..HEIGHT {
            let row: &[u8; WIDTH] = flat[i * WIDTH..(i + 1) * WIDTH].try_into().unwrap();
            unsafe {
                std::ptr::addr_of_mut!((*outp)[i]).write(row);
            }
        }

        unsafe { out.assume_init() }
    }

    /// Clear the screen to all-white.
    pub fn clear(&mut self) {
        for b in self.iter_mut() {
            *b = 0;
        }
    }

    /// Scroll the display contents up or down by a number of rows.
    ///
    /// Rows scrolled off the display are discarded, and rows added are blank (all white). When
    /// scrolling down blank rows are added at the top of the screen, or the bottom when scrolling
    /// up.
    pub fn scroll(&mut self, direction: ScrollDirection, count: usize) {
        if count == 0 {
            return;
        }
        if count >= HEIGHT {
            self.clear();
            return;
        }
        let rows = self.as_mut_rows();

        match direction {
            ScrollDirection::Up => {
                for (src_idx, dst_idx) in (count..HEIGHT).zip(0..) {
                    *rows[dst_idx] = *rows[src_idx];
                }
                for clear_row in (HEIGHT - count)..HEIGHT {
                    *rows[clear_row] = [0u8; WIDTH];
                }
            }
            ScrollDirection::Down => {
                for (src_idx, dst_idx) in (0..HEIGHT - count).rev().zip((count..HEIGHT).rev()) {
                    *rows[dst_idx] = *rows[src_idx];
                }
                for clear_row in 0..count {
                    *rows[clear_row] = [0u8; WIDTH];
                }
            }
        }
    }

    /// Write packed data to the display buffer, up to 8 pixels wide.
    ///
    /// Data is written at the given `x` and `y` coordinates, spanning the range of columns
    /// `x..x+width`, where `width` is no more than 8. Each byte of `data` fills one row of the
    /// display: `data[0]` is at `y`, `data[1]` at `y+1` and so forth.
    ///
    /// For each byte of `data`, each bit of that byte is written as one pixel, with the
    /// most-significant bit written to the leftmost X coordinate: bit 7 at `x`, bit 6 at `x+1` and
    /// so forth. If `width` is less than 8, the `8-width` least-significant bits of each byte of
    /// data are ignored. Any pixels to be written that fall outside the display area are also
    /// ignored.
    pub fn blit_packed8(&mut self, x: u8, y: u8, data: &[u8], width: u8, mode: BlitMode) {
        assert!(width <= 8, "blit_packed8 requires one byte per row");
        let row = y as usize;
        let col = x as usize;

        // Do nothing if offscreen
        if col > WIDTH || row > HEIGHT {
            return;
        }

        for (&data_row, screen_row) in data.iter().zip(&mut self.as_mut_rows()[row..]) {
            let expanded = expand_byte(data_row).to_be_bytes();
            // Copy from data to the buffer, clipping right
            let clipped_width = std::cmp::min(col + width as usize, WIDTH) - col;

            let row = &mut screen_row[col..col + clipped_width];
            match mode {
                BlitMode::Overwrite => row.copy_from_slice(&expanded[..clipped_width]),
                BlitMode::Xor => {
                    for (dst, data) in row.iter_mut().zip(expanded) {
                        // Don't use the Xor operator here since values in the pixel
                        // buffer may be neither of 0 or 1, but other values should
                        // be treated the same as 1.
                        *dst = match (*dst, data) {
                            (x, 0) | (0, x) => x,
                            (_, _) => 0,
                        }
                    }
                }
            }
        }
    }

    /// Blit a large-font character into the buffer.
    pub fn blit_char_large(&mut self, x: u8, y: u8, glyph: u8, mode: BlitMode) {
        self.blit_packed8(
            x,
            y,
            crate::charset::get_large_glyph(glyph).as_slice(),
            crate::charset::LARGE_WIDTH as u8,
            mode,
        );
    }

    /// Blit a small-font character into the buffer.
    ///
    /// The returned value is the width of the character in pixels.
    pub fn blit_char_small(&mut self, x: u8, y: u8, glyph: u8, mode: BlitMode) -> u8 {
        let (data, width) = crate::charset::get_small_glyph(glyph);
        self.blit_packed8(x, y, data.as_slice(), width, mode);
        width
    }
}

pub enum ScrollDirection {
    /// Move the screen contents down, adding blanks at the top.
    Down,
    /// Move the screen contents up, adding blanks at the bottom.
    Up,
}

pub enum BlitMode {
    Overwrite,
    Xor,
}

/// Overwrite mode is the default.
impl Default for BlitMode {
    fn default() -> Self {
        BlitMode::Overwrite
    }
}

/// Interface for interacting with a calculator display.
///
/// The associated `BufRef` and `BufRefMut` types are required to provide access
/// to the display's pixel buffer, while allowing greater flexibility than a similar
/// implementation of `AsRef`/`AsMut`: this allows a display to provide access through
/// an abstraction layer beyond raw references, such as lock guards for a shared display
/// buffer.
pub trait Display {
    type BufRef<'a>: Deref<Target = PixelBuffer>
    where
        Self: 'a;
    type BufRefMut<'a>: DerefMut<Target = PixelBuffer>
    where
        Self: 'a;

    fn buf<'a>(&'a self) -> Self::BufRef<'a>;
    fn buf_mut<'a>(&'a mut self) -> Self::BufRefMut<'a>;

    fn is_blank(&self) -> bool {
        self.buf().iter().all(|&px| px == 0)
    }
}

/// Scatter a 1bpp byte into a 1Bpp lsb-only value.
fn expand_byte(x: u8) -> u64 {
    let x = x as u64;
    // Simply shifting the value will never cause adjacent bits to overlap in the interesting
    // position of each because the interesting bits are always 8 bits apart, so we can do all
    // the shifts first and mask afterward, meaning the primary bottleneck is the number of shifts
    // the CPU can execute per cycle.
    ((x << 49) | (x << 42) | (x << 35) | (x << 28) | (x << 21) | (x << 14) | (x << 7) | x)
        & 0x0101010101010101
}

#[cfg(test)]
mod tests {
    use quickcheck_macros::quickcheck;

    use super::{BlitMode, PixelBuffer, ScrollDirection, HEIGHT, WIDTH};

    #[quickcheck]
    fn expand_byte_expands(x: u8) {
        let expanded = super::expand_byte(x).to_le_bytes();

        for bit in 0..8 {
            let expected = (x >> bit) & 1;
            let actual = expanded[bit];
            assert_eq!(expected, actual, "Expanded {:08b} -> {:?}", x, expanded);
        }
    }

    fn setup_scrolling() -> PixelBuffer {
        let mut display = PixelBuffer::default();
        // Make each row say its original index
        for (i, row) in display.as_mut_rows().iter_mut().enumerate() {
            for byte in row.iter_mut() {
                *byte = i as u8;
            }
        }

        display
    }

    #[quickcheck]
    fn scroll_up(distance: usize) {
        let mut display = setup_scrolling();
        display.scroll(ScrollDirection::Up, distance);
        let clamped = std::cmp::min(distance, HEIGHT);

        for (i, row) in display.as_rows().iter().enumerate().take(HEIGHT - clamped) {
            assert!(
                row.iter().all(|&b| b == (i + clamped) as u8),
                "Row {} should have value {}, found {}",
                i,
                i + clamped,
                row[0]
            );
        }
        for i in (HEIGHT - clamped)..HEIGHT {
            assert!(
                display.as_rows()[i].iter().all(|&b| b == 0),
                "Row {} should be all zeroes, found {}",
                i,
                display.as_rows()[i][0]
            );
        }
    }

    #[quickcheck]
    fn scroll_down(distance: usize) {
        let mut display = setup_scrolling();
        display.scroll(ScrollDirection::Down, distance);
        let clamped = std::cmp::min(distance, HEIGHT);

        for (i, row) in display.as_rows().iter().enumerate().skip(clamped) {
            assert!(
                row.iter().all(|&b| b == (i - distance) as u8),
                "Row {} should have value {}, found {}",
                i,
                i - distance,
                row[0]
            );
        }
        for i in 0..clamped {
            assert!(
                display.as_rows()[i].iter().all(|&b| b == 0),
                "Row {} should be all zeroes, found {}",
                i,
                display.as_rows()[i][0]
            );
        }
    }

    #[test]
    fn blit() {
        let mut display = PixelBuffer::default();

        display.blit_packed8(0, 0, &[0xff, 0x55], 8, BlitMode::Overwrite);
        assert_eq!(display.as_rows()[0][..8], [1; 8]);
        assert_eq!(display.as_rows()[1][..8], [0, 1, 0, 1, 0, 1, 0, 1]);

        display.blit_packed8(0, 0, &[0xff, 0x55], 8, BlitMode::Xor);
        for (i, &x) in display.iter().enumerate() {
            assert_eq!(
                x,
                0,
                "Pixel buffer ({}, {}) was nonzero",
                i % WIDTH,
                i / WIDTH
            );
        }
    }
}

/// Display the screen with Unicode Block Element characters.
///
/// Draws the display as a 48x32 character array (32 lines), where each
/// character visually represents a square of four pixels.
// TODO: use teletext sextant blocks (U+1FB0x) to get a pixel aspect
// ratio closer to 1 if typeface support is acceptable.
impl std::fmt::Debug for PixelBuffer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fn bool_pixels(x: &[u8]) -> (bool, bool) {
            let &[l, r] = x else { unreachable!() };
            (l != 0, r != 0)
        }

        // Double-struck frame outlining the whole image
        write!(f, "\u{2554}")?;
        for _ in 0..96 / 2 {
            write!(f, "\u{2550}")?;
        }
        write!(f, "\u{2557}\n")?;

        for rows in self.as_rows().chunks_exact(2) {
            write!(f, "\u{2551}")?;

            let &[top, bottom] = rows else { unreachable!() };
            let tc = top.chunks_exact(2).map(bool_pixels);
            let bc = bottom.chunks_exact(2).map(bool_pixels);

            for ((tl, tr), (bl, br)) in tc.zip(bc) {
                let c = match (tl, tr, bl, br) {
                    // Block elements, U+2580..U+259F
                    (false, false, false, false) => ' ',
                    (false, false, false, true) => '▗',
                    (false, false, true, false) => '▖',
                    (false, false, true, true) => '▄',
                    (false, true, false, false) => '▝',
                    (false, true, false, true) => '▐',
                    (false, true, true, false) => '▞',
                    (false, true, true, true) => '▟',
                    (true, false, false, false) => '▘',
                    (true, false, false, true) => '▚',
                    (true, false, true, false) => '▌',
                    (true, false, true, true) => '▙',
                    (true, true, false, false) => '▀',
                    (true, true, false, true) => '▜',
                    (true, true, true, false) => '▛',
                    (true, true, true, true) => '█',
                };
                write!(f, "{}", c)?;
            }
            write!(f, "\u{2551}\n")?;
        }

        write!(f, "\u{255a}")?;
        for _ in 0..96 / 2 {
            write!(f, "\u{2550}")?;
        }
        write!(f, "\u{255d}\n")
    }
}

#[test]
fn block_chars_display() {
    let mut buf = PixelBuffer::default();

    {
        let rows = buf.as_mut_rows();

        // Two-wide dark border around the entire screen
        for row in [0, 1, 62, 63] {
            rows[row].fill(1);
        }
        for row in rows {
            for col in [0, 1, 94, 95] {
                row[col] = 1;
            }
        }

        // Some text
        for (i, &c) in b"Hello, world!".iter().enumerate() {
            buf.blit_char_large(6 * i as u8 + 9, 8, c, BlitMode::Xor);
        }
        let lines = [
            &b"Debug output brought to"[..],
            &b"you by the Unicode"[..],
            &b"Consortium"[..],
        ];
        for (i, &line) in lines.iter().enumerate() {
            let mut x = 4;
            for &c in line {
                x += buf.blit_char_small(x, 6 * i as u8 + 40, c, BlitMode::Xor);
            }
        }
    }

    println!("{:?}", buf);
    assert_eq!(
        format!("{:?}", buf),
        "\
╔════════════════════════════════════════════════╗
║████████████████████████████████████████████████║
║█                                              █║
║█                                              █║
║█                                              █║
║█   ▐ ▐    ▜  ▜                    ▜   ▐ ▐     █║
║█   ▐▄▟▗▀▚ ▐  ▐ ▗▀▚      ▐ ▐▗▀▚▐▞▚ ▐ ▗▀▟ ▐     █║
║█   ▐ ▐▐▀▀ ▐  ▐ ▐ ▐ ▜    ▐▐▐▐ ▐▐   ▐ ▐ ▐ ▗     █║
║█   ▝ ▝ ▀▘ ▀▘ ▀▘ ▀▘ ▘     ▘▘ ▀▘▝   ▀▘ ▀▀ ▝     █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█                                              █║
║█ ▄   ▖   ▗▖    ▗     ▖ ▖       ▗▖▖ ▖ ▖        █║
║█ ▌▌▞▖▛▖▌▌▚▌▗▚▐▐▐▘▛▖▌▌▛ ▛▖▙▘▞▖▌▌▚▌▛▖▛ ▛▗▚      █║
║█ ▙▘▜▖▙▘▙▌▄▘▝▞▐▟▝▖▛ ▙▌▚ ▙▘▌ ▚▘▙▌▄▘▌▌▚ ▚▝▞      █║
║█       ▗    ▖▗    ▖▖  ▖     ▖                 █║
║█ ▌▌▞▖▌▌▐▚▐▐ ▛▐▚▗▚ ▌▌▛▖▖▞▘▞▖▞▌▞▖               █║
║█ ▞ ▚▘▙▌▐▞▗▘ ▚▐▐▝▙ ▙▌▌▌▌▚▖▚▘▚▌▜▖               █║
║█ ▗▖         ▗ ▖                               █║
║█ ▌ ▞▖▛▖▛▗▚▐▞▐▘▖▌▌▛▞▖                          █║
║█ ▚▖▚▘▌▌▟▝▞▐ ▝▖▌▙▌▌▘▌                          █║
║█                                              █║
║█                                              █║
║████████████████████████████████████████████████║
╚════════════════════════════════════════════════╝
"
    );
}
