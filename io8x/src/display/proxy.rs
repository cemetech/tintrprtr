//! Proxy access to a pixel buffer shared with other threads.

use std::sync::{RwLock, RwLockReadGuard, RwLockWriteGuard};

use super::PixelBuffer;

pub struct Display(RwLock<PixelBuffer>);

impl super::Display for Display {
    type BufRef<'a> = RwLockReadGuard<'a, PixelBuffer>;
    type BufRefMut<'a> = RwLockWriteGuard<'a, PixelBuffer>;

    fn buf<'a>(&'a self) -> Self::BufRef<'a> {
        self.0
            .read()
            .expect("unable to acquire display buffer lock for reading")
    }

    fn buf_mut<'a>(&'a mut self) -> Self::BufRefMut<'a> {
        self.0
            .write()
            .expect("unable to acquire display buffer lock for writing")
    }
}
