pub use winit;
use winit::dpi::{LogicalSize, PhysicalSize};
use winit::event_loop::EventLoop;

use pixels::{wgpu::Color, Pixels, PixelsBuilder, SurfaceTexture};

use crate::display::PixelBuffer;

pub struct Display {
    buffer: PixelBuffer,
    window: winit::window::Window,
    px: Pixels,
}

impl Display {
    pub async fn new<T, N: Into<String>>(
        event_loop: &EventLoop<T>,
        title: Option<N>,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let mut builder = winit::window::WindowBuilder::new()
            .with_min_inner_size(PhysicalSize::new(96, 64))
            .with_inner_size(LogicalSize::new(384, 256));
        if let Some(title) = title {
            builder = builder.with_title(title);
        }
        let window = builder.build(event_loop)?;

        let surface = SurfaceTexture::new(96, 64, &window);
        let px = PixelsBuilder::new(96, 64, surface)
            .enable_vsync(true)
            .clear_color(Color::BLACK)
            .build_async()
            .await?;

        Ok(Display {
            buffer: PixelBuffer::default(),
            window,
            px,
        })
    }

    pub fn window_mut(&mut self) -> &mut winit::window::Window {
        &mut self.window
    }

    pub fn draw(&mut self) {
        for (dst, &src) in self.px.get_frame().chunks_mut(4).zip(self.buffer.iter()) {
            let out = if src == 0 {
                [0xff, 0xff, 0xff, 0xff]
            } else {
                [0, 0, 0, 0xff]
            };

            dst.copy_from_slice(&out);
        }

        self.px.render().unwrap();
    }

    pub fn set_physical_size(&mut self, width: u32, height: u32) {
        self.px.resize_surface(width, height);
    }
}

impl super::Display for Display {
    type BufRef<'a> = &'a PixelBuffer;
    type BufRefMut<'a> = &'a mut PixelBuffer;

    fn buf(&self) -> &PixelBuffer {
        &self.buffer
    }

    fn buf_mut(&mut self) -> &mut PixelBuffer {
        &mut self.buffer
    }
}
