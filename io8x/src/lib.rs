//! I/O abstractions for TI-8x calculators.

pub mod charset;
pub mod display;
