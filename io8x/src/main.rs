use log::trace;
use pollster::block_on;

use io8x::display::{
    windowed::winit::{
        event::{Event, WindowEvent},
        event_loop::EventLoop,
    },
    Display,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let event_loop = EventLoop::new();

    let mut display = block_on(io8x::display::windowed::Display::new(
        &event_loop,
        None::<&'static str>,
    ))?;

    display.window_mut().set_title("io8x test");

    let mut frame_start = std::time::Instant::now();
    let mut frame_time = std::time::Duration::default();
    let (mut x, mut y) = (0, 0);
    event_loop.run(move |event, _window_target, control_flow| {
        // We use vsync, so poll will render at the native framerate
        control_flow.set_poll();

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                control_flow.set_exit();
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(size),
                ..
            } => {
                display.set_physical_size(size.width, size.height);
            }
            Event::MainEventsCleared => {
                trace!("Run simulation for {:?}", frame_time);
                display.buf_mut().as_mut_rows()[y][x] ^= 1;
                x = x + 1;
                if x >= io8x::display::WIDTH {
                    x = 0;
                    y = (y + 1) % io8x::display::HEIGHT;
                }

                display.draw();
                // We draw with vsync, so draw() blocks until the frame is presented and
                // we'll run the simulation for as long as the last frame displayed for.
                let now = std::time::Instant::now();
                frame_time = now.duration_since(frame_start);
                frame_start = now;
            }
            Event::RedrawRequested(_) => {
                display.draw();
            }
            _ => {}
        }
    });
}
