//! Windowed display smoketest
//!
//! This verifies that opening an io8x window succeeds, but otherwise doesn't
//! exercise any useful functionality. It's slightly complicated so it doesn't
//! fail to build if the required support isn't enabled at compile-time.

#[cfg(feature = "windowed")]
fn run_test() {
    use io8x::display::windowed::{winit, Display};

    let event_loop = winit::event_loop::EventLoop::new();
    let mut display = pollster::block_on(Display::new(&event_loop, Some("io8x smoketest")))
        .expect("Failed to initialize display");

    display.draw();
}

#[cfg(not(feature = "windowed"))]
fn run_test() {
    eprintln!("Windowed smoketest ignored because feature 'windowed' is disabled");
}

fn main() {
    run_test();
}
