use derive_more::From;

use crate::error::{EvalError, EvalResult};
use crate::value::Value;
use crate::var::{NumVar, StringVar};
use crate::Float;

pub type LineNumber = usize;

#[derive(Debug, Clone)]
pub struct Program<L>
where
    L: AsRef<[Node]>,
{
    lines: L,
}

impl Program<Vec<Node>> {
    pub fn from_token_bytes(
        bytes: &[u8],
    ) -> Result<Program<Vec<Node>>, Vec<crate::parse::Error<'_>>> {
        use crate::parse::{lines, TokenParser};

        let lines = lines().parse_tokens(bytes).into_result()?;
        Ok(Program { lines })
    }
}

impl<L> Program<L>
where
    L: AsRef<[Node]>,
{
    pub fn get(&self, idx: LineNumber) -> Option<&Node> {
        self.lines.as_ref().get(idx)
    }

    pub fn len(&self) -> LineNumber {
        self.lines.as_ref().len() as LineNumber
    }

    pub fn is_empty(&self) -> bool {
        self.lines.as_ref().is_empty()
    }

    /// Return an iterator over the nodes in the program.
    pub fn iter(&self) -> std::slice::Iter<Node> {
        self.lines.as_ref().iter()
    }

    pub fn find_label(&self, label: &Label) -> Option<usize> {
        self.iter().position(|node| {
            if let Node::Label(l) = node {
                l == label
            } else {
                false
            }
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Label(u8, Option<u8>);

impl Label {
    const MAX: u8 = b'Z' - b'A' + 1;
    const RANGE: std::ops::RangeInclusive<u8> = 0..=Self::MAX;

    pub(crate) fn from_letter_tokens(first: u8, second: Option<u8>) -> Label {
        assert!(
            Self::RANGE.contains(&first),
            "First letter of label must be A-Z or theta ({:?}), but was {}",
            Self::RANGE,
            first
        );
        if let Some(second) = second {
            assert!(
                Self::RANGE.contains(&second),
                "Second letter of label must be A-Z or theta ({:?}), but was {}",
                Self::RANGE,
                second
            );
        }

        Label(first, second)
    }
}

impl std::fmt::Display for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        fn byte_to_letter(x: u8) -> char {
            match x {
                0..=25 => (x + b'A') as char,
                26 => 'θ',
                _ => unreachable!(),
            }
        }

        write!(f, "Label(")?;
        write!(f, "{}", byte_to_letter(self.0))?;
        if let Some(second) = self.1 {
            write!(f, "{}", byte_to_letter(second))?;
        }
        write!(f, ")")
    }
}

/// A node is a single statement in code.
#[derive(Debug, Clone, PartialEq)]
pub enum Node {
    Blank,
    Expression(Expression),
    Store(Expression, Reference),
    If(Expression),
    Then,
    Else,
    End,
    While(Expression),
    Label(Label),
    Goto(Label),
    Disp(Vec<Expression>),
}

/// An expression yields a value.
#[derive(Debug, Clone, PartialEq, From)]
#[from(forward)]
pub enum Expression {
    Literal(Value),
    Variable(Reference),
    UnaryOp(UnaryOperator, Box<Expression>),
    BinaryOp(BinaryOperator, Box<Expression>, Box<Expression>),
}

impl Expression {
    pub fn evaluate_in_context(&self, ctx: &super::VarContext) -> EvalResult {
        use Expression::*;

        match self {
            Literal(val) => Ok(val.clone()),
            Variable(var) => {
                if let Some(val) = ctx.get(*var) {
                    Ok(val.clone())
                } else {
                    match var {
                        Reference::Ans | Reference::Number(_) => Ok(Value::from(Float::zero())),
                        Reference::String(_) => Err(EvalError::Undefined),
                    }
                }
            }
            UnaryOp(op, expr) => op.apply(expr.evaluate_in_context(ctx)?),
            BinaryOp(op, l, r) => {
                op.apply(l.evaluate_in_context(ctx)?, r.evaluate_in_context(ctx)?)
            }
        }
    }
}

impl<L, R> From<(BinaryOperator, L, R)> for Expression
where
    L: Into<Expression>,
    R: Into<Expression>,
{
    fn from(value: (BinaryOperator, L, R)) -> Self {
        Expression::BinaryOp(value.0, Box::new(value.1.into()), Box::new(value.2.into()))
    }
}

impl<T> From<(UnaryOperator, T)> for Expression
where
    T: Into<Expression>,
{
    fn from(value: (UnaryOperator, T)) -> Self {
        Expression::UnaryOp(value.0, Box::new(value.1.into()))
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum BinaryOperator {
    Add,
    Subtract,
    Multiply,
    Divide,
    /// Left operand raised to the power of the right operand.
    Exponentiate,
    /// nth root (left operand) of x (right operand)
    ///
    /// Square root if left is 2 for example, or cube root if n = 3.
    NthRoot,
    NumPermutations,
    NumCombinations,
    Or,
    Xor,
    And,
    Equal,
    NotEqual,
    GreaterThan,
    GreaterThanOrEqual,
    LessThan,
    LessThanOrEqual,
}

impl BinaryOperator {
    pub fn apply(&self, left: Value, right: Value) -> EvalResult {
        use BinaryOperator::*;

        match self {
            Add => left + right,
            Subtract => left - right,
            Multiply => left * right,
            Divide => left / right,
            Exponentiate => {
                if let Value::Number(n) = right {
                    left.pow(n)
                } else {
                    Err(EvalError::DataType)
                }
            }
            Equal => left.eq(&right),
            NotEqual => left.eq(&right).and_then(<Value as std::ops::Not>::not),
            GreaterThan => Ok(left.cmp(&right)?.is_gt().into()),
            GreaterThanOrEqual => Ok(left.cmp(&right)?.is_ge().into()),
            LessThan => Ok(left.cmp(&right)?.is_lt().into()),
            LessThanOrEqual => Ok(left.cmp(&right)?.is_le().into()),

            op => todo!("BinaryOperation {:?} on ({:?}, {:?})", op, left, right),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum UnaryOperator {
    Negate,
    /// 10 to a chosen power, as in `1e2` to mean 100.
    ///
    /// This is treated as a prefix operator, and it only accepts integer literals
    /// in the range [-99,99] as its operand.
    SciE,
    Sqrt,
    Factorial,
    MatrixTranspose,
    // TODO: some angle operations?
}

impl UnaryOperator {
    pub fn apply(&self, value: Value) -> EvalResult {
        use UnaryOperator::*;

        match self {
            Negate => -value,
            Sqrt => value.pow(Float::real(decimal_macros::d128!(0.5)).unwrap()),
            other => todo!("UnaryOperator.apply for {:?}", other),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, From)]
#[from(forward)]
pub enum Reference {
    Ans,
    Number(NumVar),
    String(StringVar),
}
