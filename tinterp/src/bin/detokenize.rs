use std::io::Read;
use std::path::PathBuf;

use clap::Parser;
use tintrprtr::tifiles;

use titokens::{DetokenizeMode, Detokenizer};

/// Reads tokens from a TI program file and outputs the program as plain text.
#[derive(Parser, Debug)]
#[clap(name = "tintrprtr-detokenize", version, author)]
struct Args {
    /// Path to a .8xp file from which to read tokens
    source_file: PathBuf,
    /// The detokenization mode to use: one of source, invisible or display
    #[clap(short, long, default_value = "source")]
    mode: DetokenizeMode,
}

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    let infile = std::fs::File::open(args.source_file)?;
    let mut input = tifiles::Reader::new(infile)?;
    let mut tokens = vec![];
    input.read_to_end(&mut tokens)?;

    let mut detokenizer = Detokenizer::ti83plus(args.mode);
    match detokenizer.detokenize(&tokens) {
        Err(e) => panic!("Failed to detokenize data: {:?}", e),
        Ok(s) => println!("{}", s),
    }
    Ok(())
}
