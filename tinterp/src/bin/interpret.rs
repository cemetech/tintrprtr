use std::fs::File;
use std::io::{BufReader, Read};
use std::path::PathBuf;

use clap::Parser;

use tintrprtr::display::{Display, Headless};
use tintrprtr::{parse_program, tifiles, Executor};
use titokens::Tokenizer;

/// Execute a TI-BASIC program
#[derive(Debug, Parser)]
#[clap(version, author)]
struct Args {
    /// Path to a .8xp or plain-text file from which to read code
    ///
    /// If plaintext, the contents will be tokenized first before
    /// executing; otherwise they will be executed directly.
    program_file: PathBuf,
}

enum Source {
    Tokens(tifiles::Reader<BufReader<File>>),
    Text(BufReader<File>),
}

impl Source {
    pub fn open<P: AsRef<std::path::Path>>(path: P) -> std::io::Result<Self> {
        let path = path.as_ref();
        let file = BufReader::new(File::open(path)?);

        let out = match path.extension() {
            Some(ext) if ext == "8xp" => Source::Tokens(tifiles::Reader::new(file)?),
            _ => Source::Text(file),
        };
        Ok(out)
    }

    pub fn read_tokens(self) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let mut out = vec![];
        match self {
            Source::Tokens(mut f) => {
                f.read_to_end(&mut out)?;
            }
            Source::Text(mut t) => {
                let mut text = vec![];
                t.read_to_end(&mut text)?;

                let tokenizer = Tokenizer::ti83plus();
                tokenizer.tokenize(&text, &mut out)?;
            }
        }

        Ok(out)
    }
}

// TODO: display with wgpu+winit

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Args = Args::parse();

    let source = Source::open(args.program_file)?;
    let tokens = source.read_tokens()?;
    let program = match parse_program(&tokens) {
        Ok(p) => p,
        Err(errs) => {
            let n = errs.len();
            for e in errs {
                eprintln!("{:?}", e);
            }
            // TODO: make this a nice error with line numbers and whatnot; ariadne is a handy
            // library, but we need to convert to a textual representation. (ariadne uses textual
            // spans). miette has similar features and abstracts out source code so we could
            // convert to text on demand.
            //
            // Extra handy conversion: can attempt to detokenize the "one of" vec from an error
            // when displaying it, to show the text strings rather than bytes.
            return Err(format!(
                "Aborting due to {} parse error{}",
                n,
                if n == 1 { "" } else { "s" }
            )
            .into());
        }
    };

    let mut executor = Executor::<_, Headless>::new(program);
    while !executor.is_terminated() {
        if let Err(e) = executor.step() {
            eprintln!("Program terminated early due to error: {:?}", e);
            break;
        }
    }

    println!("{:?}", executor.display.buf());
    Ok(())
}
