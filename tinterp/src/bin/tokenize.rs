use std::ffi::OsString;
use std::io::{Read, Seek, SeekFrom};
use std::path::PathBuf;

use clap::Parser;

use tintrprtr::tifiles::{VariableType, Writer};
use titokens::Tokenizer;

/// Reads plain text from a file and outputs tokens to a TI program file.
#[derive(Parser, Debug)]
#[clap(name = "tintrprtr-tokenize", version, author)]
struct Args {
    /// Plaintext program source to be tokenized.
    source_file: OsString,
    /// Name of 8xp file to write as output.
    out_file: PathBuf,
    /// The name of the program as appears on a calculator.
    #[clap(short = 'n', long)]
    program_name: Option<String>,
    /// Mark the output program as archived.
    #[clap(short, long)]
    archived: bool,
    /// Mark the output program as protected (does not appear in the EDIT menu).
    #[clap(short = 'r', long)]
    protected: bool,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Args = Args::parse();

    let source = {
        let mut source_file = std::fs::File::open(&args.source_file)?;
        let len = source_file.seek(SeekFrom::End(0))?;
        source_file.seek(SeekFrom::Start(0))?;

        let mut buf = Vec::with_capacity(len as usize);
        source_file.read_to_end(&mut buf)?;
        buf
    };
    let program_name = args.out_file.file_stem().unwrap().to_string_lossy();
    let outfile = std::fs::File::create(&args.out_file)?;
    let program_type = if args.protected {
        VariableType::ProtectedProgram
    } else {
        VariableType::Program
    };

    let mut out = Writer::new(outfile, program_type, &program_name, args.archived)?;
    Tokenizer::ti83plus().tokenize(&source, &mut out)?;
    out.close()?;

    Ok(())
}
