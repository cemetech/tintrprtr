//! Mapping of Unicode strings to calculator display.
//!
//! The target calculators use a custom character set that largely corresponds with
//! ASCII in the standard text ranges, but includes a variety of more exotic characters.
//! Many of those characters have reasonable Unicode equivalents (such as mathematical
//! operators and some symbols), but a small number have no single-codepoint equivalent
//! in Unicode.
//!
//! This module provides implements a bidirectional mapping between the calculator
//! character set and Unicode. Because some calculator characters require multiple
//! codepoints to represent (for instance the 'p̂' glyph), this maps single-byte calculator
//! characters to strings rather than to single Unicode characters.
//!
//! For those glyphs that have no reasonable approximation in Unicode, they are assigned
//! codepoints in the Supplementary Private Use Area-A, U+F83xx where xx is the glyph
//! index in the calculator character set.

use radix_trie::Trie;
use std::sync::LazyLock;

pub const SPACE: u8 = 0x20;
pub const ELLIPSIS: u8 = 0xce;

/// Maps unicode strings to calculator codepoints.
///
/// Recognized strings correspond to canonical tokens; alternate token strings are unlikely to
/// render the same as they would on a calculator, but canonical strings are expected to.
///
/// References:
///  * https://taricorp.gitlab.io/83pa28d/ref/lfont.html
///  * https://jacobly.com/tifonts/
///  * https://github.com/TI-Planet/z80_basic_tokens/blob/master/tokens.csv
static FROM_UNICODE: &[(&str, u8)] = &[
    ("\0", 0x00),
    ("𝑛", 0x01), // U+1D45B mathematical italic small N
    ("𝗎", 0x02), // U+1D5CE mathematical sans-serif small U
    ("𝗏", 0x03), // U+1D5CF mathematical sans-serif small V
    /* preceding U and V are the same as regular ASCII in both the large and small fonts,
    but this W is half-width in the small font */
    ("𝗐", 0x04),  // U+1D5D0 mathematical sans-serif small W
    ("►", 0x05),  // U+25BA black right-pointing pointer
    ("🠽", 0x06),  // U+1F83D upwards compressed arrow
    ("🠿", 0x07),  // U+1F83F downwards compressed arrow
    ("∫", 0x08),  // U+222B integral
    ("×", 0x09),  // U+00D7 multiplication sign
    ("□", 0x0A),  // U+25A1 white square
    ("﹢", 0x0B), // U+FE62 small plus sign
    ("·", 0x0C),  // U+00B7 middle dot
    ("ᴛ", 0x0D),  // U+1D1B latin letter small capital T
    ("³", 0x0E),  // U+00B3 superscript three
    ("𝔽", 0x0F),  // U+1D53D mathematical double-struck capital F (seems unused by any tokens)
    ("√", 0x10),  // U+221A square root
    ("⁻¹", 0x11), // U+207B superscript minus, U+00B9 superscript one
    ("²", 0x12),  // U+00B2 superscript two
    ("∠", 0x13),  // U+2220 angle
    ("°", 0x14),  // U+00B0 degree sign
    ("ʳ", 0x15),  // U+02B3 modifier letter small R
    ("ᵀ", 0x16),  // U+1D40 modifier letter capital T
    ("≤", 0x17),  // U+2264 less-than or equal to
    ("≠", 0x18),  // U+2260 not equal to
    ("≥", 0x19),  // U+2265 greater-than or equal to
    ("⁻", 0x1A),  // U+207B superscript minus
    ("ᴇ", 0x1B),  // U+1D07 latin letter small capital E
    ("→", 0x1C),  // U+2192 rightwards arrow
    ("₁₀", 0x1D), // U+2081 subscript one, U+2080 subscript zero
    ("↑", 0x1E),  // U+2191 upwards arrow
    ("↓", 0x1F),  // U+2193 downwards arrow
    (" ", 0x20),
    ("!", 0x21),
    ("\"", 0x22),
    ("#", 0x23),
    ("⁴", 0x24), // U+2074 superscript four
    ("%", 0x25),
    ("&", 0x26),
    ("'", 0x27),
    ("(", 0x28),
    (")", 0x29),
    ("*", 0x2A),
    ("+", 0x2B),
    (",", 0x2C),
    ("-", 0x2D),
    (".", 0x2E),
    ("/", 0x2F),
    ("0", 0x30),
    ("1", 0x31),
    ("2", 0x32),
    ("3", 0x33),
    ("4", 0x34),
    ("5", 0x35),
    ("6", 0x36),
    ("7", 0x37),
    ("8", 0x38),
    ("9", 0x39),
    (":", 0x3A),
    (";", 0x3B),
    ("<", 0x3C),
    ("=", 0x3D),
    (">", 0x3E),
    ("?", 0x3F),
    ("@", 0x40),
    ("A", 0x41),
    ("B", 0x42),
    ("C", 0x43),
    ("D", 0x44),
    ("E", 0x45),
    ("F", 0x46),
    ("G", 0x47),
    ("H", 0x48),
    ("I", 0x49),
    ("J", 0x4A),
    ("K", 0x4B),
    ("L", 0x4C),
    ("M", 0x4D),
    ("N", 0x4E),
    ("O", 0x4F),
    ("P", 0x50),
    ("Q", 0x51),
    ("R", 0x52),
    ("S", 0x53),
    ("T", 0x54),
    ("U", 0x55),
    ("V", 0x56),
    ("W", 0x57),
    ("X", 0x58),
    ("Y", 0x59),
    ("Z", 0x5A),
    ("θ", 0x5B), // U+03B8 greek small letter theta
    ("\\", 0x5C),
    ("]", 0x5D),
    ("^", 0x5E),
    ("_", 0x5F),
    ("‛", 0x60), // U+201B single high-reversed-9 quotation mark
    ("a", 0x61),
    ("b", 0x62),
    ("c", 0x63),
    ("d", 0x64),
    ("e", 0x65),
    ("f", 0x66),
    ("g", 0x67),
    ("h", 0x68),
    ("i", 0x69),
    ("j", 0x6a),
    ("k", 0x6b),
    ("l", 0x6c),
    ("m", 0x6d),
    ("n", 0x6e),
    ("o", 0x6f),
    ("p", 0x70),
    ("q", 0x71),
    ("r", 0x72),
    ("s", 0x73),
    ("t", 0x74),
    ("u", 0x75),
    ("v", 0x76),
    ("w", 0x77),
    ("x", 0x78),
    ("y", 0x79),
    ("z", 0x7a),
    ("{", 0x7b),
    ("|", 0x7c),
    ("}", 0x7d),
    ("~", 0x7e),
    ("⍯", 0x7f), // U+236F APL functional symbol quad not equal (never appears in tokens)
    ("₀", 0x80), // U+2080 subscript zero
    ("₁", 0x81),
    ("₂", 0x82),
    ("₃", 0x83),
    ("₄", 0x84),
    ("₅", 0x85),
    ("₆", 0x86),
    ("₇", 0x87),
    ("₈", 0x88),
    ("₉", 0x89), // .. U+2089 subscript nine
    ("Á", 0x8a), // U+00C1 latin capital letter A with acute
    ("À", 0x8b), // U+00C0 latin capital letter A with grave
    ("Â", 0x8c), // U+00C2 latin capital letter A with circumflex
    ("Ä", 0x8d), // U+00C4 latin capital letter A with diaeresis
    ("á", 0x8e), // .. latin-1 supplement continues
    ("à", 0x8f),
    ("â", 0x90),
    ("ä", 0x91),
    ("É", 0x92),
    ("È", 0x93),
    ("Ê", 0x94),
    ("Ë", 0x95),
    ("é", 0x96),
    ("è", 0x97),
    ("ê", 0x98),
    ("ë", 0x99),
    ("Í", 0x9a),
    ("Ì", 0x9b),
    ("Î", 0x9c),
    ("Ï", 0x9d),
    ("í", 0x9e),
    ("ì", 0x9f),
    ("î", 0xa0),
    ("ï", 0xa1),
    ("Ó", 0xa2),
    ("Ò", 0xa3),
    ("Ô", 0xa4),
    ("Ö", 0xa5),
    ("ó", 0xa6),
    ("ò", 0xa7),
    ("ô", 0xa8),
    ("ö", 0xa9),
    ("Ú", 0xaa),
    ("Ù", 0xab),
    ("Û", 0xac),
    ("Ü", 0xad),
    ("ú", 0xae),
    ("ù", 0xaf),
    ("û", 0xb0),
    ("ü", 0xb1),
    ("Ç", 0xb2),
    ("ç", 0xb3),
    ("Ñ", 0xb4),
    ("ñ", 0xb5),
    ("´", 0xb6), // U+00B4 acute accent
    ("`", 0xb7), // U+0060 grave accent
    ("¨", 0xb8),
    ("¿", 0xb9),
    ("¡", 0xba),
    ("α", 0xbb), // U+03B1 Greek small letter alpha
    ("β", 0xbc), // U+03B2 Greek small letter beta
    ("γ", 0xbd), // U+03B3 Greek small letter gamma
    ("Δ", 0xbe), // U+0394 Greek capital letter delta
    ("δ", 0xbf), // U+03B4 Greek small letter delta
    ("ε", 0xc0), // U+03B5 Greek small letter epsilon
    ("[", 0xc1),
    ("λ", 0xc2), // U+03BB Greek small letter lambda
    ("μ", 0xc3), // U+03BC Greek small letter mu
    ("π", 0xc4), // U+03C0 Greek small letter pi
    ("ρ", 0xc5), // U+03C1 Greek small letter rho
    ("Σ", 0xc6), // U+03A3 Greek capital letter sigma
    ("σ", 0xc7), // U+03C3 Greek small letter sigma
    ("τ", 0xc8), // U+03C4 Greek small letter tau
    ("Φ", 0xc9), // U+03A6 Greek capital letter phi
    ("Ω", 0xca), // U+03A9 Greek capital letter omega
    ("x̄", 0xcb), // x, U+0305 combining overline
    ("ȳ", 0xcc), // U+0233 latin small letter Y with macron
    ("ˣ", 0xcd), // U+02E3 modifier letter small X
    ("…", 0xce), // U+2026 horizontal ellipsis
    ("◄", 0xcf), // U+25C4 black left-pointing pointer
    ("■", 0xd0), // U+25A0 black square (unused by any token?)
    ("∕", 0xd1), // U+2215 division slash (unused by any token?)
    ("‐", 0xd2), // U+2010 hyphen (unused by any token)
    /* 0xd3 is a superscript two, but not exactly the same as 0x12 in the large font.
     * Doesn't seem to be used in any tokens; appears used for displaying computed area. */
    ("\u{f83d3}", 0xd3),
    ("˚", 0xd4), /* U+02DA ring above. Used to represent temperature, rather than angular
                  * degrees like 0x14. Looks similar, but smaller. */
    /* 0xd5 is a superscript three, same as 0x03. Possibly used for area and volume like 0xd3,
     * but never used in tokens. */
    ("\u{f83d5}", 0xd5),
    /* 0xd6 unused; unallocated in small font */
    ("\u{f83d6}", 0xd6),
    ("𝑖", 0xd7), // U+1D456 mathematical italic small I
    ("p̂", 0xd8), // p, U+0302 combining circumflex accent
    ("χ", 0xd9), // U+03C7 greek small letter chi
    ("𝙵", 0xda), // U+1D675 mathematical monospace capital F
    ("𝑒", 0xdb), // U+1D452 mathematical italic small E (Euler's number)
    ("ʟ", 0xdc), // U+029F latin letter small capital L (list name prefix)
    ("𝗡", 0xdd), // U+1D5E1 mathematical sans-serif bold capital N
    ("⸩", 0xde), // U+2E29 right double parenthesis
    ("⮕", 0xdf), // U+2B95 rightwards black arrow
    // e0 through ee are used as cursors and don't appear in tokens
    ("█", 0xe0), // U+2588 full block
    ("\u{f83e1}", 0xe1),
    ("\u{f83e2}", 0xe2),
    ("\u{f83e3}", 0xe3),
    ("\u{f83e4}", 0xe4),
    ("\u{f83e5}", 0xe5),
    ("\u{f83e6}", 0xe6),
    ("\u{f83e7}", 0xe7),
    ("╲", 0xe8), // U+2572 box drawings light diagonal upper left to lower right
    ("\u{f83e9}", 0xe9),
    ("◥", 0xea), // U+25E5 black upper right triangle
    ("◣", 0xeb), // U+25E3 black lower left triangle
    ("⊸", 0xec), // U+22B8 multimap
    ("∘", 0xed), // U+2218 ring operator
    ("⋱", 0xee), // U+22F1 down right diagonal ellipsis
    /* Compare EF and F0 to 06 and 07: the tail on EF/F0 is one pixel longer
     * than on 06/07, so the latter are encoded as compressed arrows. */
    ("🡅", 0xef), // U+1F845 upwards heavy arrow
    ("🡇", 0xf0), // U+1F847 downwards heavy arrow
    ("░", 0xf1), // U+2591 light shade
    ("$", 0xf2),
    ("🡁", 0xf3), // U+1F841 upwards heavy compressed arrow
    ("ß", 0xf4), // U+00DF latin small letter sharp S
    ("\u{f83f5}", 0xf5),
    ("⁄", 0xf6), // U+2044 fraction slash (MathPrint)
                 /* remaining bytes are unused and render as a 3x3 pixel filled box */
];

static CHARS_TO_GLYPH: LazyLock<Trie<String, u8>> = LazyLock::new(|| {
    FROM_UNICODE
        .iter()
        .map(|&(s, b)| (String::from(s), b))
        .collect()
});

/// An iterator over calculator glyphs in a string.
///
/// ```
/// # use tintrprtr::charset::ToGlyphs;
/// let mut glyphs = ToGlyphs::new("🡅ʟROOF");
///
/// assert_eq!(
///     (&mut glyphs).collect::<Vec<_>>(),
///     &[0xef, 0xdc, 0x52, 0x4f, 0x4f, 0x46]
/// );
/// assert!(glyphs.remaining().is_empty());
/// ```
///
/// To verify that the entire input string was mapped to calculator characters,
/// use [`remaining`](ToGlyphs::remaining).
pub struct ToGlyphs<'a>(&'a str);

impl<'a> ToGlyphs<'a> {
    pub fn new(s: &'a str) -> Self {
        ToGlyphs(s)
    }

    /// Return the unconsumed portion of the iterated string.
    ///
    /// If [`next`](Iterator::next) has returned `None` and this string is non-empty,
    /// that implies that no character set equivalent exists for the leading character(s)
    /// in the unconsumed portion.
    pub fn remaining(&self) -> &'a str {
        self.0
    }
}

impl<'a> Iterator for ToGlyphs<'a> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        use radix_trie::TrieCommon;

        let matched = CHARS_TO_GLYPH.get_ancestor(self.0)?;
        self.0 = &self.0[matched.key().unwrap().len()..];
        Some(*matched.value().unwrap())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.remaining().len()))
    }
}

/// An iterator that converts calculator glyphs into strings.
///
/// ```
/// # use tintrprtr::charset::FromGlyphs;
/// let mut iter = FromGlyphs::new(&[0xef, 0xdc, b'R', b'O', b'O', b'F']);
/// assert_eq!(
///     &(&mut iter).collect::<String>(),
///     "🡅ʟROOF"
/// );
/// assert!(iter.remaining().is_empty());
/// ```
pub struct FromGlyphs<'a>(std::slice::Iter<'a, u8>);

impl<'a> FromGlyphs<'a> {
    pub fn new(bytes: &'a [u8]) -> Self {
        FromGlyphs(bytes.iter())
    }

    /// Return the unconsumed portion of the iterated bytes.
    ///
    /// If [`next`](Iterator::next) has returned `None` and this slice is non-empty,
    /// that implies that the first byte of the slice is not mapped to a glyph on
    /// the calculator.
    pub fn remaining(&self) -> &'a [u8] {
        self.0.as_slice()
    }

    fn translate(value: u8) -> Option<&'static str> {
        match FROM_UNICODE.get(value as usize) {
            Some((s, _)) => Some(s),
            None => None,
        }
    }
}

impl<'a> Iterator for FromGlyphs<'a> {
    type Item = &'static str;

    fn next(&mut self) -> Option<Self::Item> {
        Self::translate(*self.0.next()?)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.0.len()))
    }
}

impl<'a> DoubleEndedIterator for FromGlyphs<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        Self::translate(*self.0.next_back()?)
    }
}

/// FromGlyphs uses a simple looking in FROM_UNICODE that requires a string
/// be at the same index as the glyph number.
#[test]
fn map_data_has_simple_indexes() {
    for (i, &(_, j)) in FROM_UNICODE.iter().enumerate() {
        assert_eq!(
            i, j as usize,
            "Glyph {j:#x} should be at index {j} but was at {i}"
        )
    }
}

#[test]
fn all_canonical_chars_represented() {
    use radix_trie::TrieCommon;
    use titokens::{TokenKind, Tokenizer};

    assert!(
        CHARS_TO_GLYPH.get("").is_none(),
        "Empty string must not have a token"
    );

    for (bytes, string, kind) in Tokenizer::ti83plus().iter_tokens() {
        // Only canonical tokens are mapped, and newlines are control-only
        if kind != TokenKind::Canonical || string == "\n" {
            continue;
        }

        let mut remaining = string;
        while !remaining.is_empty() {
            match CHARS_TO_GLYPH.get_ancestor(remaining) {
                None => panic!("Characters from {:?} in token {:?} (bytes {:x?}) have no equivalent in charset mapping",
                               remaining, string, bytes),
                Some(t) => {
                    let key = t.key().unwrap();
                    assert!(remaining.starts_with(key));
                    assert!(key.len() > 0);
                    remaining = &remaining[key.len()..];
                }
            }
        }
    }
}
