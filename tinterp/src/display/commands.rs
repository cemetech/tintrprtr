use std::iter::repeat;

use titokens::{DetokenizeMode, Detokenizer};

use super::homescreen::Homescreen;
use crate::charset::{ToGlyphs, ELLIPSIS, SPACE};
use crate::value::Value;

pub fn disp<'a, V: IntoIterator<Item = &'a Value>>(values: V, homescreen: &mut Homescreen) {
    for value in values {
        homescreen.erase_line();

        match value {
            Value::Number(num) => {
                // Numbers are displayed right-aligned, ellipsized if longer than 16 characters.
                // TODO: hook up display modes for fixed precision display,
                // scientific/engineering notation.
                let mut glyphs = ToGlyphs::new(&format!("{}", num))
                    .take(Homescreen::WIDTH + 1)
                    .collect::<Vec<_>>();

                if glyphs.len() > Homescreen::WIDTH {
                    glyphs.truncate(Homescreen::WIDTH - 1);
                    glyphs.push(ELLIPSIS);
                }
                let padding = repeat(SPACE).take(Homescreen::WIDTH - glyphs.len());

                for glyph in padding.chain(glyphs) {
                    homescreen.blit_glyphs(&[glyph]);
                }
            }
            Value::String(tokens) => {
                // Strings are displayed left-aligned, with an ellipsis added at the end of the
                // line if it would overflow.
                let mut detokenizer = Detokenizer::ti83plus(DetokenizeMode::Display);
                let mut tokens = tokens.as_slice();

                for token in tokens {
                    let s = detokenizer.detokenize_u16(*token).unwrap_or_else(|| {
                        panic!("Token {:#06x} in string could not be detokenized", token)
                    });

                    let mut glyphs = ToGlyphs::new(s);

                    for glyph in &mut glyphs {
                        if homescreen.blit_glyphs(&[glyph]) == 0 {
                            // Reached right side of screen; ellipsize
                            debug_assert_eq!(homescreen.x() as usize, Homescreen::WIDTH);
                            homescreen.set_x(Homescreen::WIDTH as u8 - 1);
                            let blitted = homescreen.blit_glyphs(&[ELLIPSIS]);
                            assert_eq!(blitted, 1);
                        }
                    }
                    assert!(
                        glyphs.remaining().is_empty(),
                        "all characters in token {:?} must be displayable",
                        glyphs.remaining()
                    );
                }
            }
        }

        // A newline always follows a displayed value, and is never added without any value
        // (Disp with no items only displays the homescreen).
        homescreen.new_line();
    }
}

/// Disp with no values doesn't change the homescreen's contents.
#[test]
fn disp_nothing() {
    let mut hs = Homescreen::default();

    disp([], &mut hs);

    assert_eq!((hs.x(), hs.y()), (0, 0));
    assert!(
        hs.text_shadow.iter().flatten().all(|&c| c == SPACE),
        "text_shadow should remain blank after bare 'Disp' command"
    );
}

/// Strings are displayed left-aligned and ellipsized if too wide.
#[test]
fn disp_string() {
    use titokens_macros::tokenize;
    let mut hs = Homescreen::default();

    disp(
        [
            Value::string_from_bytes(&b"ABCDEF"[..]).unwrap(),
            Value::string_from_bytes(&b"0123456789ABCDEF"[..]).unwrap(),
            Value::string_from_bytes(&tokenize!("NormalFloatsqrt(pi^^2)OMIT")[..]).unwrap(),
        ]
        .iter(),
        &mut hs,
    );
    assert_eq!((hs.x(), hs.y()), (0, 3), "Should display three lines");
    assert_eq!(&hs.text_shadow[0], b"ABCDEF          ");
    assert_eq!(&hs.text_shadow[1], b"0123456789ABCDEF");
    assert_eq!(&hs.text_shadow[2], b"NormalFloat\x10(\xc4\x12\xce");
}

/// Numbers are displayed right-aligned and ellipsized if too wide.
#[test]
fn disp_number() {
    use crate::Float;

    let mut hs = Homescreen::default();
    disp(
        [
            Value::from(123),
            Value::from(Float::new(2, -3).unwrap()),
            Value::from(Float::new(987654321, 123456789).unwrap()),
        ]
        .iter(),
        &mut hs,
    );

    assert_eq!(&hs.text_shadow[0], b"             123");
    assert_eq!(&hs.text_shadow[1], b"            2-3\xd7");
    assert_eq!(&hs.text_shadow[2], b"987654321+12345\xce");
}
