use super::{BlitMode, Display};
use crate::charset::SPACE;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Homescreen {
    pub text_shadow: [[u8; Self::WIDTH]; Self::HEIGHT],
    cursor: (u8, u8),
}

impl Default for Homescreen {
    fn default() -> Self {
        Homescreen {
            text_shadow: [[SPACE; Self::WIDTH]; Self::HEIGHT],
            cursor: (0, 0),
        }
    }
}

impl Homescreen {
    pub const WIDTH: usize = 16;
    pub const HEIGHT: usize = 8;

    pub fn clear(&mut self) {
        *self = Default::default();
    }

    pub fn x(&self) -> u8 {
        self.cursor.0
    }

    pub fn set_x(&mut self, x: u8) {
        self.cursor.0 = x;
    }

    pub fn y(&self) -> u8 {
        self.cursor.1
    }

    pub fn set_y(&mut self, y: u8) {
        self.cursor.1 = y;
    }

    /// Scroll the screen down one line.
    ///
    /// This removes the topmost line, moving the one below it up and so forth. The bottom line of
    /// the screen is cleared.
    pub fn scroll(&mut self) {
        self.text_shadow.copy_within(1.., 0);
        *self.text_shadow.last_mut().unwrap() = [SPACE; Self::WIDTH];
    }

    /// Erase the line the cursor is on, returning the cursor to the beginning of the line.
    pub fn erase_line(&mut self) {
        self.text_shadow[self.y() as usize] = [SPACE; Self::WIDTH];
        self.set_x(0);
    }

    /// Move the cursor to a new line.
    ///
    /// If the cursor is at the bottom of the screen, it will be [`scroll`]ed to a new line.
    pub fn new_line(&mut self) {
        let (_, mut y) = self.cursor;

        y += 1;
        if y >= Self::HEIGHT as u8 {
            y = Self::HEIGHT as u8 - 1;
            self.scroll();
        }

        self.cursor = (0, y);
    }

    pub fn blit_glyphs(&mut self, glyphs: &[u8]) -> usize {
        let (x, y) = self.cursor;

        let mut count = 0;
        for (dst, &glyph) in self.text_shadow[y as usize][x as usize..]
            .iter_mut()
            .zip(glyphs)
        {
            *dst = glyph;
            count += 1;
        }
        self.cursor.0 += count as u8;
        count
    }

    pub fn draw<D: Display>(&self, display: &mut D) {
        let mut buf = display.buf_mut();

        buf.clear();
        for (y, row) in self.text_shadow.iter().enumerate() {
            for (x, &c) in row.iter().enumerate() {
                buf.blit_char_large(6 * x as u8, 8 * y as u8, c, BlitMode::Overwrite);
            }
        }
    }
}

#[test]
fn new_line() {
    let mut hs = Homescreen::default();

    // With some text onscreen, it stays there when we go to a new line.
    hs.blit_glyphs(b"ABC");
    hs.new_line();
    assert_eq!(hs.cursor, (0, 1));
    assert_eq!(&hs.text_shadow[0], b"ABC             ");

    // More text that will remain after we scroll.
    hs.blit_glyphs(b"ZYX");

    // Moving onto the last line, still haven't scrolled.
    hs.cursor = (0, 6);
    hs.new_line();
    assert_eq!(&hs.text_shadow[0], b"ABC             ");
    assert_eq!(hs.cursor, (0, 7));
    // But going down past the end of the screen will scroll.
    hs.new_line();
    assert_eq!(&hs.text_shadow[0], b"ZYX             ");
    assert_eq!(&hs.text_shadow[7], b"                ");
    assert_eq!(hs.cursor, (0, 7));
}

/// Homescreen::draw() copies textShadow to the screen correctly.
#[test]
fn draw() {
    use crate::display::Headless;

    let mut ref_display = Headless::new();
    {
        let buf = ref_display.buf_mut();
        for x in 0..16 {
            buf.blit_char_large(
                6 * x,
                0,
                char::from_digit(x as u32, 16).unwrap() as u8,
                BlitMode::Overwrite,
            );
        }
        for y in 1..8 {
            buf.blit_char_large(
                90,
                8 * y,
                char::from_digit(y as u32, 16).unwrap() as u8,
                BlitMode::Overwrite,
            );
        }
    }

    let mut display = Headless::new();
    let mut hs = Homescreen::default();
    hs.text_shadow[0] = *b"0123456789abcdef";
    for row in 1..8 {
        hs.text_shadow[row][15] = char::from_digit(row as u32, 16).unwrap() as u8;
    }
    hs.draw(&mut display);

    println!(
        "Expected:\n{:?}\nActual:\n{:?}",
        ref_display.buf(),
        display.buf()
    );
    assert!(ref_display == display);
}
