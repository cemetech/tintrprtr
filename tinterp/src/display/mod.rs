//! Handling of the calculator display.
//!
//! Output is primarily through bindings of the io8x crate. Other submodules
//! handle the aspects of display that are specific to TI-BASIC.

pub mod commands;
pub mod homescreen;

pub use io8x::display::*;
