use thiserror::Error;

pub type EvalResult = Result<crate::Value, EvalError>;

#[derive(Debug, PartialEq, Eq, Clone, Error)]
pub enum EvalError {
    #[error("not enough arguments provided to function")]
    FunctionMissingArgument,
    /// Attempted to use a value of the wrong type.
    ///
    /// Example causes include attempting to store a matrix to a numeric variable
    /// (expected number, got matrix) or attempting to take a substring of a list
    /// (expected string, got list).
    #[error("value(s) of this/these types are invalid for this operation")]
    DataType,
    /// The divisor of an operation was zero.
    #[error("attempted to divide by zero")]
    DivideByZero,
    /// An input value was outside the permitted range.
    #[error("value is outside the supported range")]
    Domain,
    /// There is insufficient free memory to complete the operation.
    #[error("not enough memory")]
    Memory,
    /// A value would have exceeded the maximum range of representable numbers.
    #[error("numeric value is too large to represent")]
    Overflow,
    /// An expression referred to a variable that is not set
    #[error("referred to an undefined variable")]
    Undefined,
    /// `ERR:LABEL`: a Goto or Menu command referred to a label that does not exist.
    #[error("label {0} is not defined")]
    Label(crate::ast::Label),
}
