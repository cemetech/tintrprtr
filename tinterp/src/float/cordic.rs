//! The CORDIC algorithm for iterative approximation of trigonometric operations.
//!
//! CORDIC comes from [Volder's 1959 publication][volder] describing the algorithm as used in
//! a digital navigation computer, replacing analog computers in that use (though the general
//! techniques were first published by Henry Briggs in _Trigonometria Britannica_, published 1633).
//! Several years after Volder, [Meggit described "pseudo-multiplication" and "pseudo-division"
//! operations][meggit] working on similar principles but targeting base-10 computation.
//! [Andraka's more recent summary of CORDIC][andraka] is more accessible to the modern reader,
//! geared to hardware designers but easily adapted to software as well.
//!
//! [volder]: https://doi.org/10.1109/AFIPS.1959.57
//! [meggit]: https://doi.org/10.1147/rd.62.0210
//! [andraka]: http://www.andraka.com/files/crdcsrvy.pdf
//!
//! CORDIC is widely used in pocket calculators, documented by TI (though with no mention of the
//! specific algorithms) and HP, described in detail by multiple articles in the [Hewlett-Packard
//! Journals][hp]:
//!
//!  * David S. Cochran, "Algorithms and Accuracy in the HP-35", volume 23 number 10 (1972)
//!  * William E. Egbert, "Personal Calculator Algorithms"
//!    * "I: Square roots", volume 28, number 9 (May 1977)
//!    * "II: Trigonometric functions", volume 28, number 10 (June 1977)
//!    * "III: Inverse Trigonometric Functions", volume 29, number 3 (November 1977)
//!    * "IV: Logarithmic Functions", volume 29, number 8 (April 1978)
//!
//! [ti]: https://education.ti.com/en/customer-support/knowledge-base/ti-83-84-plus-family/product-usage/11693
//! [hp]: http://hparchive.com/hp_journals
//!
//! The HP implementation in particular borrows from Meggit's work, being described in terms of
//! pseudo-multiplication and pseudo-division. This is useful because this code also operates on
//! decimal numbers rather than binary, so the usual base-2 optimizations of CORDIC are not
//! efficient. It is somewhat difficult to relate Egbert's description of the algorithms to the
//! other descriptions of CORDIC however: Hermann Schmid's description of decimal CORDIC in
//! [_Decimal Computation_ (Wiley-Interscience, 1974)][schmid] is much more accessible, and
//! provides the basis for this implementation.
//!
//! [schmid]: https://archive.org/details/decimalcomputati0000schm/
//!
//! ---
//!
//! Schmid defines the recurrence relation as follows:
//!
//! * n = (i, j): the iteration count
//! * Xₙ₊₁ = Kᵢ(Xₙ ± 10⁻ⁱ * Yₙ): the X coordinate at each step
//! * Yₙ₊₁ = Kᵢ(Yₙ ∓ 10⁻ⁱ * Xₙ): the Y coordinate at each step
//! * Kᵢ = cos(𝛼ᵢ)⁻¹
//! * 𝛼ᵢ = tan⁻¹(10⁻ⁱ): the angular step at each order of magnitude
//!
//! In binary CORDIC only one iteration per order of magnitude is required for the algorithm to
//! converge, but in decimal each order of magnitude (i in the iteration count) must be evaluated
//! up to 9 times in order to achieve correct results: the value of j covers the range `1..=9` for
//! each value of i with the exception of i=1, where only one iteration is required.
//!
//! Kₙ becomes the total "gain" over n iterations, the product of all Kᵢ from i=0 to i=n. Provided
//! the number of iterations of the algorithm is constant, this value is also a constant. The value
//! of Kᵢ is equivalent to √(10⁻²ⁱ + 1) (which rapidly converges toward 1 as i increases), so:
//!
//! Kₙ = √(2) * √(10⁻² + 1)⁹ * √(10⁻⁴ + 1)⁹ * ... ≅ 1.479648
//!
//! By factoring out the multiplication by Kᵢ and dividing by Kₙ at the end, we can get a
//! simplified expression suitable for shift-and-add implementation:
//!
//! * Xₙ₊₁ / Kₙ = Xₙ ± 10⁻ⁱ * Yₙ
//! * Yₙ₊₁ / Kₙ = Yₙ ∓ 10⁻ⁱ * Xₙ
//!
//! This can be confirmed as correct by comparing with Andraka's description, where this version of
//! the algorithm differs in the number of iterations and the constant twos are replaced with tens.
//! The choice of addition or subtraction at each stage is made to ensure the accumulated sum of 𝛼ᵢ
//! values moves toward the desired angle (Andraka describes this as the coefficient dᵢ).
//!
//! Hyperbolic trig functions can be computed in nearly the same way as circular ones, but the
//! choice of addition or subtraction in the recurrence is inverted and the constants are slightly
//! different: Schmid describes this in detail in Chapter 8.
// TODO: the fixed-point approach doesn't really work in floating-point: 83+ says sin(1e-20) =
// 1e-20, but this algorithm would round that to zero?
//
// Extra investigation: calculator says sin(1e-5) = 1e-5, though appears to differ in guard digits:
// sin(1e-5)-(1e-5) = -1.666e-16, and sin(1e-6) == 1e-6 (guard digits rounded off). Similarly
// cos(1e-6) == 1. So it seems the calculator does CORDIC to fairly low precision (about 6 decimal
// places) and assumes that for small angles <1e-5 sin(x) = x and cos(x) = 1. A change of less than
// 1e-6 does still affect the result when the input is larger however: sin(1-1e-7) differs from
// sin(1) by 5.4e-8.
//
// Mike Sebastian's "Calculator Forensics" is interesting because it includes some discussion of
// guard digits and the sensitivity of a series of trigonometric operations to accuracy. He notes
// that asin(acos(atan(tan(cos(sin(9 degrees)))))) should be an identity function but rarely is.
// All of the TI-82 through 86 evaluate that to 8.9999999695957 degrees.
//
// Floating-point CORDIC: https://doi.org/10.1109/ARITH.1993.378100

// TODO: remove when actually used
#![allow(unused)]

use decimal::d128;
use decimal_macros::d128;

/// atan(1), or sqrt(2)/2
const ARCTAN_1: d128 = d128!(7.853981633974483096156608458198757e-1);

/// Precomputed values of atan(10⁻ⁱ)
///
/// [`d128`] captures 34 significant digits, so these values are computed with large precision
/// and rounded to 34 significant digits.
///
/// ```python
/// from sympy import *
/// for i in range(14):
///     print(N(atan(Pow(10, -i)), 80))
/// ```
const ARCTAN_TABLE: [d128; 14] = [
    d128!(9.966865249116202737844611987802059e-2),
    d128!(9.999666686665238206340116209279549e-3),
    d128!(9.999996666668666665238096349205440e-4),
    d128!(9.999999966666666866666665238095249e-5),
    d128!(9.999999999666666666686666666665238e-6),
    d128!(9.999999999996666666666668666666667e-7),
    d128!(9.999999999999966666666666666866667e-8),
    d128!(9.999999999999999666666666666666687e-9),
    d128!(9.999999999999999996666666666666667e-10),
    d128!(9.999999999999999999966666666666667e-11),
    d128!(9.999999999999999999999666666666667e-12),
    d128!(9.999999999999999999999996666666667e-13),
    d128!(9.999999999999999999999999966666667e-14),
    d128!(9.999999999999999999999999999666667e-15),
];

/// Inverse gain of the entire process, Kₙ⁻¹
///
/// Most operations should multiply by this inverse gain to get useful results.
///
/// ```python
/// from sympy import *
///
/// i = symbols('i', integer=True)
///
/// N(
///     1 / (
///         sqrt(2) * product(
///             Pow(sqrt(1 + Pow(10, -2 * i)), 9),
///             (i, 1, 13)
///         )
///     ),
///     n=34
/// )
/// ```
///
/// The upper bound chosen for i must be equal to the length of [`ARCTAN_TABLE`], because
/// that size determines the number of iterations that are run.
// TODO make this accurate once the iteration count is sorted
const INV_GAIN: d128 = d128!(0.6758361580438929665188057995590642);

/// π/4
const PI_4: d128 = d128!(7.853981633974483096156608458198757e-1);
/// π/2
const PI_2: d128 = d128!(1.570796326794896619231321691639751);

const NEGATIVE_ONE: d128 = d128!(-1);

fn rotator(mut x: d128, mut y: d128, mut θ: d128) -> (d128, d128, d128) {
    assert!(θ >= d128::zero());
    assert!(θ <= PI_2);

    fn rotate_step(
        x: d128,
        y: d128,
        θ: d128,
        #[allow(non_snake_case)] Δθ: &d128,
        shift: usize,
    ) -> (d128, d128, d128) {
        if θ.is_positive() {
            (x - (y >> shift), y + (x >> shift), θ - Δθ)
        } else {
            (x + (y >> shift), y - (x >> shift), θ + Δθ)
        }
    }

    // i = 0 runs only once
    (x, y, θ) = dbg!(rotate_step(x, y, θ, /* atan(1) = */ &PI_4, 0));

    for (i, arctan) in (1..).zip(ARCTAN_TABLE.iter()) {
        for _j in 1..=9 {
            (x, y, θ) = dbg!(rotate_step(x, y, θ, arctan, i));
        }
    }

    (x, y, θ)
}

#[ignore]
#[test]
fn rotate_sin_cos() {
    let mut x = INV_GAIN;
    let mut y = d128::zero();
    let mut angle = PI_2;

    dbg!(&x, &y, &angle);
    (x, y, angle) = rotator(x, y, angle);

    // TODO seem to need to go down to 1e-16 in the arctan table for this test to round
    // to 0 at 1e-14. Compare actual TI result (do they actually get zero, or just round off at
    // 10 digits of precision?).
    fn roundoff(x: d128) -> d128 {
        // quantize seems to work here because the expected range is 0-1; generally rounding
        // to 14 significant digits needs to be based on logb().
        let template = d128!(1e-14);
        x.quantize(template)
        //x >> 20 << 20
    }
    assert_eq!(dbg!(roundoff(x)), d128::zero(), "cos(pi/2) = 0");
    assert_eq!(roundoff(y), d128!(1), "sin(pi/2) = 1");
    assert_eq!(roundoff(angle), d128::zero());
}

/// Taylor series evaluation of sin(x)
#[allow(unused)]
fn sin_series_expansion(radians: d128) -> d128 {
    // sin(x) = x - x^3/3! + x^5/5! - x^7/7! + ...
    let mut out = d128::zero();

    // By experiment, 15 terms is enough to get 14 digits of precision at pi/4
    const FACTORIALS: [d128; 15] = [
        d128!(1),  // 1!
        d128!(2),  // 2!
        d128!(6),  // 3!
        d128!(24), // 4!...
        d128!(120),
        d128!(720),
        d128!(5040),
        d128!(40320),
        d128!(362880),
        d128!(3628800),
        d128!(39916800),
        d128!(479001600),
        d128!(6227020800),
        d128!(87178291200),
        d128!(1307674368000),
        /*
        d128!(20922789888000),
        d128!(355687428096000),
        d128!(6402373705728000),
        d128!(121645100408832000),
        */
    ];

    for (i, factorial) in FACTORIALS.iter().enumerate().step_by(2) {
        // i = 0, 2, 4, ...
        let term = radians.pow(d128::from(i as i32 + 1)) / factorial;

        if i % 4 == 0 {
            out += term;
        } else {
            out -= term;
        }
    }

    out
}
