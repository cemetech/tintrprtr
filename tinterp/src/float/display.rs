use decimal::d128;
#[cfg(test)]
use decimal_macros::d128;
use std::fmt::{Debug, Display, Formatter};

use super::{round_to_digits, Float};

// TODO: implement support for scientific and engineering notation

fn format_float_sci_internal(
    exponent: isize,
    mut mantissa: impl ExactSizeIterator<Item = char>,
    f: &mut Formatter,
) -> std::fmt::Result {
    debug_assert!((-99..=99).contains(&exponent));

    // Digits
    write!(
        f,
        "{}",
        mantissa.next().expect("Should have at least one sig fig")
    )?;
    if mantissa.len() > 0 {
        write!(f, ".")?;
    }
    for digit in mantissa {
        write!(f, "{}", digit)?;
    }

    // Exponent
    write!(f, "\u{1d07}" /* ᴇ */)?;
    if exponent.is_negative() {
        write!(f, "\u{207b}" /* ⁻ */)?;
    }
    write!(f, "{}", exponent.abs())
}

/// Format a float in auto+float mode.
fn format_float_auto<T: AsRef<d128> + Debug>(value: T, f: &mut Formatter) -> std::fmt::Result {
    use std::cmp::{max, min};
    use std::iter::repeat;

    if f.precision().is_some() {
        todo!("Support fixed precision in range 0..=9");
    }

    let (negative, exponent, mantissa) = round_to_digits(value, Float::DISPLAY_PRECISION)
        .reduce()
        .to_bcd();
    // mantissa is right-aligned in the array, padded with zeroes on the left.
    // We never display less than one character, even if the value is zero.
    let sig_figs = max(
        1,
        mantissa.len() - mantissa.iter().take_while(|&&d| d == 0).count(),
    );
    let mantissa = mantissa[mantissa.len() - sig_figs..]
        .iter()
        .map(|&digit| char::from_digit(digit as u32, 10).unwrap());
    // Number of digits before the decimal point; zero or negative if the
    // value is less than 1.
    let integer_digits = sig_figs as isize + exponent as isize;

    if negative {
        write!(f, "\u{207b}")?;
    }

    // Values inside the range `0.001 .. 1e10` are displayed as decimal numbers;
    // all others in normal mode use exponential notation.
    if (-2..=10).contains(&integer_digits) {
        // Construct an iterator over the digits to display, including any leading
        // zeroes that come before the decimal point
        let leading_zeroes = repeat('0').take(-min(integer_digits, 0) as usize);
        let digits = leading_zeroes.chain(mantissa);
        // When there are leading zeroes, the decimal point must come first. Otherwise
        // count down to the decimal point's position.
        let mut until_dc = max(integer_digits, 0);

        for digit in digits {
            if until_dc == 0 {
                write!(f, ".")?;
            }
            until_dc -= 1;

            write!(f, "{}", digit)?;
        }
    } else {
        return format_float_sci_internal(integer_digits - 1, mantissa, f);
    }

    Ok(())
}

#[test]
fn test_format_float_auto() {
    fn f(value: d128) -> String {
        struct Auto(d128);
        impl Display for Auto {
            fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
                format_float_auto(self.0, f)
            }
        }

        format!("{}", Auto(value))
    }

    // Positive/negative
    assert_eq!(f(d128!(0)), "0");
    assert_eq!(f(d128!(-1)), "\u{207b}1");
    // Rounding to Float::DISPLAY_PRECISION digits
    assert_eq!(f(d128!(1) - d128!(1e-10)), ".9999999999");
    assert_eq!(f(d128!(1) - d128!(1e-11)), "1");
    // Breakpoints to exponential notation
    assert_eq!(f(d128!(1e-3)), ".001");
    assert_eq!(f(d128!(9e-4)), "9ᴇ\u{207b}4");
    assert_eq!(f(d128!(9999999999)), "9999999999");
    assert_eq!(f(d128!(10000000000)), "1ᴇ10");
    // A very long string
    assert_eq!(
        f(d128!(-9.999999999e-99)),
        "\u{207b}9.999999999\u{1d07}\u{207b}99"
    );
}

/// Format as a floating-point number.
///
/// Specified precision is supported, to force display of the given number of
/// significant digits.
impl Display for Float {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        if self.is_real() {
            format_float_auto(self.0, f)
        } else if self.is_imaginary() {
            format_float_auto(self.1, f)?;
            write!(f, "𝑖")
        } else {
            debug_assert!(
                self.is_complex(),
                "{:?} is not real nor imaginary so must be complex",
                self
            );

            format_float_auto(self.0, f)?;
            // a+bi with negative b uses a minus sign rather than negative sign,
            // so select the right one and then format the absolute value.
            write!(f, "{}", if self.1.is_negative() { '-' } else { '+' })?;
            format_float_auto(self.1.abs(), f)?;
            write!(f, "𝑖")
        }
    }
}
