pub mod cordic;
mod display;

use decimal::d128;
use decimal_macros::d128;
use std::fmt::{Debug, Formatter};

use crate::EvalError;

/// A complex floating-point number.
///
/// This is the core TI-BASIC numeric type. It is based on decimal floats because TI floats
/// are also a decimal format, so this should also use decimal floats to avoid precision loss
/// that could otherwise occur when running code designed for a decimal float system on one using
/// binary floats.
///
/// TI-float allows exponents between -99 and 99 inclusive, and has an 14-digit mantissa (where only
/// 10 digits are visible to the user; the remaining 4 are guard digits). The decimal128 type used
/// in the implementation here has a 34-digit mantissa and supports exponents in the range
/// `[-6143,6144]`: much more capable than TI floats.
///
/// The construction functions like [`Float::new`] check the range of the inputs, converting values
/// smaller than [epsilon](Float::epsilon) to zero and converting values outside the range of
/// positive or negative [`max`](Float::max) to overflow errors.
#[derive(Copy, Clone)]
pub struct Float(d128, d128);

impl Debug for Float {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Float({} + {}i)", self.0, self.1)
    }
}

const FLOAT_MAX: d128 = d128!(9.9999999999999e99);
const FLOAT_EPSILON: d128 = d128!(1e-99);

impl Float {
    /// The largest representable value for a TI-float.
    ///
    /// The minimum permitted valid is this but negated.
    pub const fn max() -> d128 {
        FLOAT_MAX
    }
    /// The smallest representable value for a TI-float.
    ///
    /// Smaller values truncate to zero.
    pub const fn epsilon() -> d128 {
        FLOAT_EPSILON
    }
    /// Number of digits used to represent a TI-float.
    ///
    /// While the calculator keeps this many digits internally, it will
    /// never display more than DISPLAY_PRECISION digits.
    pub const PRECISION: u8 = 14;

    /// Number of digits shown when displaying a TI-float.
    pub const DISPLAY_PRECISION: u8 = 10;

    pub fn new<T: Into<d128>, U: Into<d128>>(real: T, imag: U) -> Result<Self, EvalError> {
        let (mut real, mut imag) = (real.into(), imag.into());
        // Test for overflow
        let max = Self::max();
        if real.abs() > max || imag.abs() > max {
            return Err(EvalError::Overflow);
        }
        // Clamp subnormal values to zero
        let epsilon = Self::epsilon();
        if real.abs() < epsilon {
            real = d128::zero();
        }
        if imag.abs() < epsilon {
            imag = d128::zero();
        }
        // Round results off to the emulated precision
        Ok(Float(
            round_to_digits(real, Self::PRECISION),
            round_to_digits(imag, Self::PRECISION),
        ))
    }

    pub fn zero() -> Self {
        Float(d128::zero(), d128::zero())
    }

    /// Make a float with zero real component and the given imaginary component.
    pub fn imaginary<T: Into<d128>>(value: T) -> Result<Self, EvalError> {
        Float::new(d128::zero(), value.into())
    }

    /// Make a float with zero imaginary component and the given real component.
    pub fn real<T: Into<d128>>(value: T) -> Result<Self, EvalError> {
        Float::new(value.into(), d128::zero())
    }

    /// Return whether self is exactly equal to zero.
    pub fn is_zero(&self) -> bool {
        self.0.is_zero() && self.1.is_zero()
    }

    /// Return whether self is a real number: it has zero imaginary component.
    pub fn is_real(&self) -> bool {
        self.1.is_zero()
    }

    /// Return whether self is an imaginary number: it has zero real component and
    /// nonzero imaginary component.
    pub fn is_imaginary(&self) -> bool {
        self.0.is_zero() && !self.1.is_zero()
    }

    /// Return whether self is a complex number: it has nonzero real and imaginary components.
    pub fn is_complex(&self) -> bool {
        !self.0.is_zero() && !self.1.is_zero()
    }

    /// Return self with the imaginary component set to zero.
    pub fn to_real(&self) -> Self {
        todo!()
    }

    /// Return the real component of self as the provided type, if the conversion can be done
    /// losslessly.
    pub fn to_real_as<T>(&self) -> Option<T>
    where
        T: FromFloat,
    {
        todo!()
    }

    /// Return the floor of self.
    pub fn floor(&self) -> Self {
        todo!()
    }

    /// Return the value of self truncated to the integer part only.
    pub fn truncate(&self) -> Self {
        todo!()
    }

    /// Return the absolute value of self.
    pub fn abs(&self) -> Self {
        todo!()
    }

    pub fn pow<O: Into<Float>>(&self, power: O) -> Result<Self, EvalError> {
        // TODO Real vs a+bi mode matters here: if the input is complex (even with zero
        // imaginary component) the output can be complex, but if the input is real and
        // the system is in real mode then the root of a negative number (complex output)
        // is an error.

        // Exponentiation of complex numbers is rather complex!
        //  * https://brilliant.org/wiki/complex-exponentiation/
        //  * https://socratic.org/questions/how-do-i-find-the-negative-power-of-a-complex-number#110207
        //  * https://www.cuemath.com/algebra/square-root-of-complex-number/
        //  * https://github.com/python/cpython/blob/e6391e08bff775a3c10707fd2cfce6963e6ae429/Objects/complexobject.c#L129
        //
        // The general definition is most easily expressed in polar form, doing
        // trigonometric transformations on the input values before converting
        // back to rectangular form.

        let exp = power.into();
        if self.is_real() && exp.is_real() && (!self.0.is_negative() || exp.0.is_integer()) {
            // real ** real fast path: the result is guaranteed to be a real number
            Float::real(self.0.pow(exp.0))
        } else if exp.is_zero() {
            Float::real(d128::from(1i32))
        } else if self.is_zero() {
            if !exp.1.is_zero() || exp.0.is_negative() {
                Err(EvalError::Domain)
            } else {
                Ok(Float::zero())
            }
        } else {
            todo!("Implement complex exponentiation");
            // Useful reference: https://github.com/python/cpython/blob/f1f3af7b8245e61a2e0abef03b2c6c5902ed7df8/Objects/complexobject.c#L129
            /*
            vabs = hypot(a.real,a.imag);
            len = pow(vabs,b.real);
            at = atan2(a.imag, a.real);
            phase = at*b.real;
            if (b.imag != 0.0) {
                len /= exp(at*b.imag);
                phase += b.imag*log(vabs);
            }
            r.real = len*cos(phase);
            r.imag = len*sin(phase);
             */
        }
    }

    /// Compute the sine of self, in radians.
    pub fn sin(&self) -> Result<Self, EvalError> {
        if !self.is_real() {
            // Trig functions are defined only for real numbers (in general they are defined
            // for complex numbers, but TI-BASIC does not).
            return Err(EvalError::DataType);
        }
        todo!()
    }
}

impl<T: Into<d128>> From<T> for Float {
    fn from(x: T) -> Self {
        // TODO this probably needs to check bounds
        Float(x.into(), d128::zero())
    }
}

impl PartialEq for Float {
    // 1 + 1e-11 displays as 1, but subtracting 1 from that yields 1e-11 so
    // we know it's rounded off. (1+1e-11)=1 evaluates to true though, so
    // we know comparisons are based on rounding to the displayed digits.
    fn eq(&self, rhs: &Float) -> bool {
        round_to_digits(self.0, Float::DISPLAY_PRECISION)
            == round_to_digits(rhs.0, Float::DISPLAY_PRECISION)
            && round_to_digits(self.1, Float::DISPLAY_PRECISION)
                == round_to_digits(rhs.1, Float::DISPLAY_PRECISION)
    }
}

impl Eq for Float {}

/// Compare the values of real numbers.
///
/// In general complex numbers are impossible to generate an ordering for,
/// and TI-BASIC doesn't differentiate between complex and imaginary numbers
/// so any comparison between non-real values is undefined.
impl PartialOrd for Float {
    fn partial_cmp(&self, rhs: &Float) -> Option<std::cmp::Ordering> {
        use std::cmp::Ordering::*;

        if !self.is_real() || !rhs.is_real() {
            None
        } else {
            let cmp = round_to_digits(self.0, Self::DISPLAY_PRECISION)
                .compare(round_to_digits(rhs.0, Self::DISPLAY_PRECISION));
            if cmp.is_negative() {
                Some(Less)
            } else if cmp.is_zero() {
                Some(Equal)
            } else if cmp.is_positive() {
                Some(Greater)
            } else {
                unreachable!("Float components should never be NaN: {:?}/{:?}", self, rhs);
            }
        }
    }
}

/// Make a real number from its decimal string representation.
///
/// Panics if the provided string is not a valid float.
impl std::str::FromStr for Float {
    type Err = EvalError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let x = match d128::from_str(s) {
            Ok(x) => x,
            Err(e) => panic!("{:?} is not a valid float: {:?}", s, e),
        };

        Float::real(x)
    }
}

impl<T: Into<d128> + Clone> PartialEq<T> for Float {
    fn eq(&self, other: &T) -> bool {
        let other: d128 = other.clone().into();
        other == self.0 && self.is_real()
    }
}

// TODO: Eq is valid for Float because it doesn't have NaNs, but should verify
// that contained values are also Eq.

impl<T: Into<Float>> std::ops::Add<T> for Float {
    type Output = Result<Float, EvalError>;

    fn add(self, other: T) -> Self::Output {
        let other = other.into();
        Float::new(self.0 + other.0, self.1 + other.1)
    }
}

impl<T: Into<Float>> std::ops::Mul<T> for Float {
    type Output = Result<Float, EvalError>;

    fn mul(self, other: T) -> Self::Output {
        let other = other.into();
        Float::new(
            // (real * real) and (imag * imag) are both real components, and the latter is
            // always negated.
            self.0 * other.0 - self.1 * other.1,
            // (real * imag) in either order is an imaginary value that maintains sign.
            self.0 * other.1 + self.1 * other.0,
        )
    }
}

impl<T: Into<Float>> std::ops::Div<T> for Float {
    type Output = Result<Float, EvalError>;

    /// (a + bi) / (c + di)
    fn div(self, other: T) -> Self::Output {
        let other = other.into();
        let denominator = (other.0 * other.0) + (other.1 * other.1);
        if denominator.is_zero() {
            Err(EvalError::DivideByZero)
        } else {
            Float::new(
                // real: (ac + bd) / (cc + dd)
                ((self.0 * other.0) + (self.1 * other.1)) / denominator,
                // imag: (bc - ad) / (cc + dd)
                ((self.1 * other.0) + (self.0 * other.1)) / denominator,
            )
        }
    }
}

impl<T: Into<Float>> std::ops::Rem<T> for Float {
    type Output = Result<Float, EvalError>;

    fn rem(self, rhs: T) -> Self::Output {
        todo!()
    }
}

impl std::ops::Neg for Float {
    type Output = Self;

    fn neg(self) -> Self::Output {
        // Input is assumed to be in range, so simply negating never causes
        // output to be out of range; no need to use ::new.
        Float(-self.0, -self.1)
    }
}

/// d128 values outside TI-float range trigger overflow errors
#[test]
fn overflow_propagates() {
    let large_value = d128!(1e100);
    assert_eq!(Float::real(large_value), Err(EvalError::Overflow));
    assert_eq!(Float::real(-large_value), Err(EvalError::Overflow));
    assert_eq!(Float::imaginary(large_value), Err(EvalError::Overflow));
    assert_eq!(Float::imaginary(-large_value), Err(EvalError::Overflow));

    assert_eq!(
        Float::real(d128!(1e87)).unwrap() + Float::max(),
        Err(EvalError::Overflow)
    );
}

/// d128 values smaller than TI-float epsilon round to zero
#[test]
fn underflow_propagates() {
    let small_value = d128!(1e-100);
    assert_eq!(Float::real(small_value), Float::real(0i32));
    assert_eq!(Float::real(-small_value), Float::real(0i32));
}

/// Round a floating-point value to the specified number of significant digits.
///
/// A digit count of zero is accepted but not useful; it always returns 0 but
/// with the same exponent as the input.
fn round_to_digits<T: AsRef<d128>>(value: T, digits: u8) -> d128 {
    let value = value.as_ref();
    // Fast path: if the input has fewer significant digits than we want,
    // don't need to do anything.
    if value.digits() <= digits as u32 {
        return *value;
    }

    let exp = value.logb();
    // Normalize to an integer part <10; 123.45 -> 1.2345
    let norm = value.scaleb(-exp);
    // Round to the specified number of digits
    let template = d128!(1).scaleb(d128::from(-(digits as i32 - 1)));
    let rounded_norm = norm.quantize(template);
    // Scale back to the original exponent
    rounded_norm.scaleb(exp)
}

#[test]
fn test_round_to_digits() {
    assert_eq!(round_to_digits(d128!(123.456789), 5), d128!(123.46));

    assert_eq!(
        round_to_digits(d128!(5.4321012345e30), 8),
        d128!(5.4321012e30)
    );

    assert_eq!(round_to_digits(d128!(1000), 0), d128::zero());

    // Fast path
    assert_eq!(round_to_digits(d128!(123), 5), d128!(123));
}

pub trait FromFloat: Sized {
    fn from_float(f: Float) -> Option<Self>;
}

impl FromFloat for i32 {
    fn from_float(f: Float) -> Option<Self> {
        todo!()
    }
}
