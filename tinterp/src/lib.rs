// TODO: remove once things work okay
#![allow(unused)]

pub use tifiles;

use display::homescreen::Homescreen;
use display::Display as DisplayAdapter;
pub use float::Float;

use ast::Reference;
use error::{EvalError, EvalResult};
use value::Value;

pub mod ast;
pub mod charset;
pub mod display;
pub mod error;
pub mod float;
pub mod parse;
mod random;
mod token;
pub mod value;
pub mod var;

pub fn parse_program(
    token_bytes: &[u8],
) -> Result<ast::Program<Vec<ast::Node>>, Vec<parse::Error<'_>>> {
    ast::Program::from_token_bytes(token_bytes)
}

#[derive(Debug)]
pub struct Executor<Lines, Display>
where
    Display: DisplayAdapter,
    Lines: AsRef<[ast::Node]>,
{
    terminated: bool,
    program: ast::Program<Lines>,
    current_line: ast::LineNumber,
    control_flow_stack: ControlFlowStack,
    pub vars: VarContext,
    pub display: Display,
    pub homescreen: Homescreen,
}

/// The set of variables accessible to BASIC programs.
#[derive(Clone, Debug, PartialEq)]
pub struct VarContext {
    ans: Option<Value>,
    /// Numeric vars A-Z, theta and 𝑛
    // TODO ideally these are Floats rather than Values, but that makes get() taking Reference difficult.
    // (having a Reference-like trait with associated type would work, changing
    // get() to get<R: TypedReference, V: Into<R>>(&self, var: V) -> Option<R::Ty>)
    nums: [Option<Value>; 28],
    strings: [Option<Value>; 256],
}

/// Construct a blank context.
impl Default for VarContext {
    fn default() -> Self {
        VarContext {
            ans: None,
            nums: Default::default(),
            strings: [const { None }; 256],
        }
    }
}

impl VarContext {
    /// Get the value of a variable, if defined.
    ///
    /// If a variable is not currently defined, returns None.
    pub fn get<R: Into<Reference>>(&self, var: R) -> Option<&Value> {
        match var.into() {
            Reference::Ans => self.ans.as_ref(),
            Reference::Number(num) => self.nums[num as usize].as_ref(),
            Reference::String(s) => self.strings[s.index()].as_ref(),
        }
    }

    // TODO handling dynamically-allocated variables should have an Entry-like type
    pub fn get_mut<R: Into<Reference>>(&mut self, var: R) -> &mut Option<Value> {
        match var.into() {
            Reference::Ans => &mut self.ans,
            Reference::Number(num) => &mut self.nums[num as usize],
            Reference::String(s) => &mut self.strings[s.index()],
        }
    }

    /// Set the value of a variable.
    ///
    /// Returns an error if the provided Value cannot be stored in a variable of the type
    /// corresponding to the provided Reference. On success, the previous value of the variable
    /// is returned in the same way as [`VarContext::get`].
    pub fn set<R: Into<Reference>>(
        &mut self,
        var: R,
        value: Value,
    ) -> Result<Option<Value>, EvalError> {
        use ast::Reference::*;

        let previous_value = match (var.into(), value) {
            // Ans can take any value; it's uniquely polymorphic
            (Ans, val) => self.ans.replace(val),
            (Number(num), val @ Value::Number(_)) => self.nums[num as usize].replace(val),
            (String(str), val @ Value::String(_)) => self.strings[str.index()].replace(val),
            (Number(_), _other) | (String(_), _other) => return Err(EvalError::DataType),
        };

        Ok(previous_value)
    }
}

/// The control flow stack pairs block-based control flow constructs with their ending tokens.
///
/// When a conditional or looping construct is encountered, it gets pushed onto the stack
/// alongside a flag indicating whether the block is to be executed ("taken") or skipped
/// ("not taken"). A block will be executed as long as every containing block is also
/// executed and the block's condition (if any) evaluates to an appropriate value.
///
/// When executing code, whether a line is actually executed depends on whether it is inside
/// a block that is to be executed. When blocks are not taken, the lines inside them are
/// inspected to determine if they affect control flow (which is required in order to find
/// which `End` statements terminate any given block when they may be nested) but not
/// evaluated, and they still affect the control flow stack.
///
/// End tokens pop a level off the stack, because they mark the end of a block.
/// Then and Else blocks replace preceding blocks at the same scope, so the stack will
/// proceed as follows for each line in an example `If 1:Then:Else:End`:
///
/// 1. `If 1` [If taken]
/// 2. `Then` [Then taken]
/// 3. `Else` [Else not-taken]
/// 4. `End` <empty>
///
/// In general an Else block will inherit the opposite taken state of the preceding Then
/// block, but this also requires that all preceding blocks are taken. For simplicity (to avoid
/// scanning the entire stack), whether the current line of code will be executed or skipped is
/// based only on the taken state of the topmost control flow stack entry; this means that
/// Else blocks may have the same taken state as the preceding Then if they are inside another
/// not-taken block. For example, tracking the stack through some nested conditions:
///
/// 1. start []
/// 2. `If 0` [If not-taken]: condition evaluates to false, block is not taken.
/// 3. `Then` [Then not-taken]
/// 4. `If 1` [Then not-taken, If not-taken]: new `If` is pushed but condition is not evaluated
///    (because top of stack is not-taken) and taken state is copied to the new entry.
/// 5. `Then` [Then not-taken, Then not-taken]: `Then` block replaces `If`, copying its taken state.
/// 6. `Else` [Then not-taken, Else not-taken]: `Else` replaces `Then`, but remains not-taken
///    because the stack entry below it is not-taken.
/// 7. `End` [Then not-taken]: pop inner block.
/// 8. `Else` [Else taken]: preceding stack entry is taken (no entry is equivalent to a taken
///    block because the top level of a program is always executed) so taken state of new Else
///    block is the inverse of the Then it replaces.
/// 9. `End` []: pop final block
///
/// `If` statements may be single-line and lack a closing `End`, but in such a configuration
/// they can still be nested; this is why `Then` is a different state from `If`, because any
/// non-control-flow line of code (or `End`) terminates all open single-line `If`s. For example:
///
/// 1. `If 1` [If taken]
/// 2. `If 0` [If taken, If not-taken]
/// 3. `Then` [If taken, Then not-taken]
/// 4. `End` []: first `If` must also be popped because the contained statement has also ended.
///
/// ---
///
/// EOS has a number of misbehaviors where it accepts code that would be incorrect if transformed
/// into a control flow graph. For instance:
///
///  * `Then` that's not immediately preceded by `If` is allowed inside not-taken blocks.
///  * If a block is exited by using `Goto`, the next encountered `End` jumps back to the
///    loop if it should, and otherwise continues to the line after the `End` that was executed
///    (not the one that actually matches the loop's beginning).
///
/// It's possible to emulate these bugs with a control flow stack, whereas transformation
/// into a true CFG would prevent them (which is undesirable for bug-compatibility with EOS).
#[derive(Debug, PartialEq, Clone, Default)]
struct ControlFlowStack(Vec<(ControlFlowClass, bool)>);

impl ControlFlowStack {
    fn top(&self) -> Option<&(ControlFlowClass, bool)> {
        self.0.last()
    }

    fn top_mut(&mut self) -> Option<&mut (ControlFlowClass, bool)> {
        self.0.last_mut()
    }

    /// Return true if the topmost stack entry is taken, or the stack is empty.
    fn top_is_taken(&self) -> bool {
        self.top().map_or(true, |(_, taken)| *taken)
    }

    /// Push an entry onto the stack, with branch-taken state inherited from the top entry
    /// and combined with the result of evaluating the given expression.
    fn push_with_expr(
        &mut self,
        kind: ControlFlowClass,
        vars: &VarContext,
        expr: &ast::Expression,
    ) -> Result<(), EvalError> {
        self.0.push((
            kind,
            self.top_is_taken() && expr.evaluate_in_context(vars)?.as_bool()?,
        ));
        Ok(())
    }

    /// Push an entry onto the stack.
    fn push(&mut self, kind: ControlFlowClass, taken: bool) {
        self.0.push((kind, self.top_is_taken() && taken));
    }

    /// Remove the top entry from the stack and return it.
    fn pop(&mut self) -> Option<(ControlFlowClass, bool)> {
        self.0.pop()
    }

    /// Pop any bare `If` blocks from the control flow stack.
    ///
    /// This is used to close blocks for both when handling `End` and for non-control-flow lines,
    /// as both of those constitute a statement that closes an unbracketed block.
    fn close_bare_if_blocks(&mut self) {
        while matches!(self.top(), Some((ControlFlowClass::If, _))) {
            self.pop();
        }
    }
}

impl<Lines, Display> Executor<Lines, Display>
where
    Display: DisplayAdapter + Default,
    Lines: AsRef<[ast::Node]>,
{
    pub fn new(program: ast::Program<Lines>) -> Self {
        Executor {
            terminated: false,
            program,
            current_line: 0,
            control_flow_stack: Default::default(),
            vars: Default::default(),
            display: Default::default(),
            homescreen: Default::default(),
        }
    }

    pub fn is_terminated(&self) -> bool {
        self.terminated
    }

    /// Consume self, returning the value of Ans.
    pub fn into_ans(self) -> Option<Value> {
        self.vars.get(Reference::Ans).cloned()
    }

    pub fn get_line(&self) -> Option<&ast::Node> {
        self.program.get(self.current_line)
    }

    pub fn step(&mut self) -> Result<(), EvalError> {
        let Executor { ref program, .. } = self;

        let line = match program.get(self.current_line) {
            None => {
                assert!(self.terminated);
                return Ok(());
            }
            Some(l) => l,
        };

        let execute_this = self.control_flow_stack.top_is_taken();
        let mut autoclose = false;
        let mut jump_to_line: Option<usize> = None;
        match line {
            ast::Node::If(expr) => {
                self.control_flow_stack
                    .push_with_expr(ControlFlowClass::If, &self.vars, expr);
            }
            ast::Node::Then => {
                match self.control_flow_stack.top_mut() {
                    Some((cond @ ControlFlowClass::If, _)) => {
                        *cond = ControlFlowClass::Then;
                    }
                    Some(_) => {
                        // This isn't actually an error with TI-OS if not taken,
                        // but it doesn't make sense either.
                        // If 0:Then:Then:End is valid and skipped.
                        todo!("Return error for misplaced Then");
                    }
                    None => {
                        todo!("Return error for empty CFS");
                    }
                }
            }
            ast::Node::Else => {
                let then_taken = match self.control_flow_stack.pop() {
                    Some((ControlFlowClass::Then, taken)) => taken,
                    _ => todo!("Return error for misplaced Else"),
                };
                // If the parent block is not taken, this one isn't either. Otherwise, it
                // gets the opposite taken state from the Then it replaces.
                self.control_flow_stack
                    .push(ControlFlowClass::Else, !then_taken);
            }
            ast::Node::End => {
                if execute_this {
                    // Bare if statements can be followed by End which operates on the
                    // containing block, rather awkwardly: "While 1:If 1:End" is an
                    // infinite loop and "While 1:If 0:End" is not because the End
                    // doesn't get executed (and execution stops on reaching EOF).
                    self.control_flow_stack.close_bare_if_blocks();
                }
                match self.control_flow_stack.pop() {
                    None => todo!("Return error for End without any open block"),
                    Some((ControlFlowClass::While(predicate, line), true)) => {
                        // Execution of a loop always continues following the End that
                        // was executed, rather than at the End that lexically matches
                        // the loop beginning so we need to evaluate the loop condition
                        // here.
                        if predicate.evaluate_in_context(&self.vars)?.as_bool()? {
                            jump_to_line = Some(line);
                        }
                    }
                    Some((ControlFlowClass::If, taken)) => {
                        assert!(
                            !taken,
                            "Untaken If block should have been previously closed"
                        );
                    }
                    Some(
                        (ControlFlowClass::While(_, _), false)
                        | (ControlFlowClass::Then | ControlFlowClass::Else, _),
                    ) => { /* Just pop off the CFS */ }
                }
                // This counts as an executed line with regard to If without Then:
                // in `If / If / Then / End / A`, `A` is outside the first If block.
                autoclose = true;
            }
            ast::Node::While(predicate) => {
                self.control_flow_stack.push_with_expr(
                    ControlFlowClass::While(predicate.clone(), self.current_line),
                    &self.vars,
                    predicate,
                );
            }
            _ => {
                // Not a statement affecting the control flow stack
                autoclose = true;
            }
        }

        if execute_this {
            let value = match program.get(self.current_line).unwrap() {
                /* Control flow statements that don't affect Ans, handled above. */
                ast::Node::If(_)
                | ast::Node::Then
                | ast::Node::Else
                | ast::Node::End
                | ast::Node::While(_) => None,
                ast::Node::Blank => None,
                ast::Node::Expression(expr) => Some(expr.evaluate_in_context(&self.vars)?),
                ast::Node::Store(expr, dest) => {
                    let value = expr.evaluate_in_context(&self.vars)?;
                    self.vars.set(*dest, value.clone())?;
                    // The expression value gets copied to Ans even when storing
                    Some(value)
                }
                ast::Node::Label(_) => None,
                ast::Node::Goto(dest) => {
                    if let Some(i) = self.program.find_label(dest) {
                        jump_to_line = Some(i);
                    } else {
                        return Err(EvalError::Label(*dest));
                    }
                    None // No change to Ans
                }
                ast::Node::Disp(exprs) => {
                    let values = exprs
                        .iter()
                        .map(|expr| expr.evaluate_in_context(&self.vars))
                        .collect::<Result<Vec<Value>, EvalError>>()?;
                    display::commands::disp(&values, &mut self.homescreen);

                    // Disp always draws the homescreen to the display (switching off
                    // the graph or whatever other screen) and doesn't touch Ans.
                    self.homescreen.draw(&mut self.display);
                    None
                }
            };
            if let Some(v) = value {
                // Store result to Ans if this line affects Ans
                self.vars.set(Reference::Ans, v)?;
            }
        }

        if autoclose {
            // Any non-control-flow line will end a base If statement
            // (one that doesn't have a Then).
            self.control_flow_stack.close_bare_if_blocks();
        }

        self.current_line = jump_to_line.unwrap_or(self.current_line + 1);
        self.terminated = self.current_line >= self.program.len();
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
enum ControlFlowClass {
    If,
    Then,
    Else,
    // TODO: would be nice to take a ref to an Expression, not sure if that's easy
    While(ast::Expression, ast::LineNumber),
}
