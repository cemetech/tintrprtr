use std::str::FromStr;

use crate::ast::{BinaryOperator, Expression, UnaryOperator};
use crate::error::EvalError;
use crate::value::Value;
use crate::{ast, var, Float};
use chumsky::input::ValueInput;
use chumsky::pratt::postfix;
use chumsky::prelude::*;
use decimal::d128;

#[cfg(test)]
use titokens_macros::tokenize;

use crate::token::{BytesToTokens, Token};
// An alias like this would be nice, but isn't currently stable.
// TODO(https://github.com/rust-lang/rust/issues/63063) use an alias.
//type Parser<'a, O, I> = impl chumsky::Parser<'a, I, O, extra::Err<Rich<'a, Token>>>;

pub type Error<'a> = Rich<'a, Token>;
pub type Result<'a, T> = ParseResult<T, Error<'a>>;

/// Parser extension for convenient parsing of bytes as tokens.
pub(crate) trait TokenParser<'a, B, O>
where
    B: AsRef<[u8]> + 'a,
{
    /// Interpret a collection of bytes as tokens, then parse it.
    fn parse_tokens(&self, input: B) -> Result<'a, O>;
}

impl<'a, B, O, P> TokenParser<'a, B, O> for P
where
    B: AsRef<[u8]> + 'a,
    P: Parser<
        'a,
        chumsky::input::Stream<BytesToTokens<B, &'static titokens::DetokenizerSpec>>,
        O,
        //Error<'a>,
        extra::Err<Rich<'a, Token>>,
    >,
{
    fn parse_tokens(&self, input: B) -> Result<'a, O> {
        self.parse(chumsky::input::Stream::from_iter(BytesToTokens::new(input)))
    }
}

fn sto<'a, I>() -> impl Parser<'a, I, Token, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    just(0x04).labelled("→")
}

fn comma<'a, I>() -> impl Parser<'a, I, Token, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    just(0x2b).labelled(",")
}

pub(crate) fn lines<'a, I>(
) -> impl Parser<'a, I, Vec<ast::Node>, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    let eol = just(0x3e).labelled(":").or(just(0x3f).labelled("newline"));

    choice((
        just(0xce)
            .labelled("If")
            .as_context()
            .ignore_then(expression())
            .map(ast::Node::If),
        select!(
            0xcf => ast::Node::Then,
            0xd0 => ast::Node::Else,
            0xd4 => ast::Node::End,
        ),
        just(0xd1)
            .labelled("While")
            .as_context()
            .ignore_then(expression())
            .map(ast::Node::While),
        just(0xd6)
            .labelled("Lbl")
            .as_context()
            .ignore_then(label())
            .map(ast::Node::Label),
        just(0xd7)
            .labelled("Goto")
            .as_context()
            .ignore_then(label())
            .map(ast::Node::Goto),
        just(0xde)
            .labelled("Disp")
            .as_context()
            .ignore_then(expression().separated_by(comma()).collect::<Vec<_>>())
            .map(ast::Node::Disp),
        // TODO: {list}->{var}+ stores to the list with the name constructed from
        // the var tokens. This is the one situation where multiple identifiers can
        // be provided.
        // TODO {string}->{list} causes the list to be set to the equation represented
        // by the string, indicated by a mark in the stat editor. The equation must
        // evaluate to a list, otherwise a DataType error is raised on use. (Interesting
        // use: `"L1->L2` makes L2 always have the same contents as L1)
        expression()
            .then_ignore(sto())
            .then(variable())
            .labelled("variable assignment")
            .as_context()
            .map(|(expr, dest)| ast::Node::Store(expr, dest)),
        expression()
            .map(ast::Node::Expression)
            .or_not()
            .map(|e| e.unwrap_or(ast::Node::Blank)),
    ))
    .separated_by(eol)
    .collect()
}

// TODO: scientific notation is nasty!
// `1e100` is ERR:SYNTAX
// `1eX` is also ERR:SYNTAX
// `Xe2` is valid
//
// so it seems like the e suffix on a value must be an integer literal

// TODO: tracking line numbers (and maybe column) should be done in the parser,
// using extra::Full<Rich<'a, Token>, MyState, ()> rather than extra::Err
// (or not? Perhaps that should be encoded in spans)

/// A real number: any non-negative floating-point value.
fn real<'a, I>() -> impl Parser<'a, I, d128, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    let digit = || {
        one_of(0x30..=0x39)
            .map(|tok| char::from_u32(tok as u32).unwrap())
            .labelled("digit")
    };
    let decpnt = just(0x3a).labelled("decimal point");

    // A natural number is a positive integer or 0.
    let natural = || {
        digit()
            .repeated()
            .at_least(1)
            .collect::<String>()
            .map(|s| d128::from_str(&s))
            .unwrapped()
    };

    choice((
        // Integer part with decimal point and optional fractional part.
        natural().then(decpnt.ignore_then(natural().or_not())),
        // Required decimal point and fractional part (no integer part).
        decpnt
            .ignore_then(natural())
            .map(|n| (d128::zero(), Some(n))),
        // Integer only.
        natural().map(|n| (n, None)),
    ))
    .map(|(integer, fractional)| {
        match fractional {
            None => integer,
            Some(mut fractional) => {
                // Move the decimal point to the front, since this was interpreted as
                // an integer.
                fractional = fractional.scaleb(-d128::from(fractional.digits()));
                integer + fractional
            }
        }
    })
}

/// A real or imaginary (but not complex) floating-point value.
///
/// Complex numbers are not a syntatic construct, and are only created by
/// applying operators to real or imaginary values.
fn number<'a, I>() -> impl Parser<'a, I, Float, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    // 𝑖 (mathematical italic small i) used to indicate imaginary numbers
    let imaginary_i = just(0x2c).labelled("𝑖");

    choice((
        // Numeric literals may overflow the valid range of a float which
        // is an error, but we need to use validate() to denote that we
        // successfully parsed something and it happened to be forbidden.
        real().validate(|val, ex, emitter| match Float::real(val) {
            Err(e) => {
                emitter.emit(Rich::custom(ex.span(), e));
                Float::zero()
            }
            Ok(flt) => flt,
        }),
        imaginary_i
            .to(Float::imaginary(1).expect("imaginary number i should always be a valid Float")),
    ))
}

fn string<'a, I>() -> impl Parser<'a, I, Value, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    let quote = just(0x2a).labelled("\"");

    quote
        .ignore_then(
            none_of([
                0x3f, /* newline */
                0x2a, /* " */
                0x04, /* sto */
            ])
            .repeated()
            .collect(),
        )
        .then_ignore(quote.or_not())
        .map(Value::String)
}

#[test]
fn parse_reals() {
    use decimal_macros::d128;

    assert_eq!(real().parse_tokens(b"123").unwrap(), d128!(123));
    assert_eq!(real().parse_tokens(b"123\x3a456").unwrap(), d128!(123.456));
    assert_eq!(real().parse_tokens(b"\x3a1").unwrap(), d128!(0.1));
}

#[test]
fn test_parse_numbers() {
    use decimal_macros::d128;

    // Integers work
    assert_eq!(
        number().parse_tokens(tokenize!("42")).unwrap(),
        Float::real(42).unwrap()
    );

    // Real numbers are fine
    assert_eq!(
        number().parse_tokens(tokenize!("3.14")).unwrap(),
        Float::real(d128!(3.14)).unwrap()
    );

    // Imaginary numbers too (only i is an imaginary literal; other values are
    // always the result of an operation, often implicit multiplication in the
    // expression "3i" or similar).
    assert_eq!(
        number().parse_tokens(tokenize!("[i]")).unwrap(),
        Float::imaginary(1).unwrap(),
    );

    // Extremely large values are a parse-time overflow error.
    let mut googol = [b'0'; 101];
    googol[0] = b'1';
    assert_eq!(
        number().parse_tokens(googol).into_result(),
        Err(vec![Rich::custom(
            SimpleSpan::from(0..googol.len()),
            EvalError::Overflow
        )])
    );
}

/// A reference to Ans.
fn ans<'a, I>() -> impl Parser<'a, I, ast::Reference, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    just(0x72u16).labelled("Ans").to(ast::Reference::Ans)
}

// A letter, representing a regular numeric variable (A-Z or theta)
fn letter<'a, I>() -> impl Parser<'a, I, u8, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    one_of(0x41..=0x5b).labelled("A-Zθ").map(|x| x as u8 - 0x41)
}

/// A reference to a variable, but not Ans.
fn variable<'a, I>() -> impl Parser<'a, I, ast::Reference, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    use var::{NumVar, StringVar};

    // TODO: also need other types, including:
    //  * Entire lists
    //  * Matrices
    //  * List or matrix elements
    //  * Strings
    choice((
        letter().map(|x| ast::Reference::from(NumVar::try_from(x).unwrap())),
        one_of(0xaa00..=0xaa63)
            .labelled("Str1-Str0")
            .map(|x| ast::Reference::from(StringVar::from(x as u8))),
    ))
    .labelled("variable name")
}

fn label<'a, I>() -> impl Parser<'a, I, ast::Label, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    letter()
        .then(letter().or_not())
        .map(|(x, y)| ast::Label::from_letter_tokens(x, y))
}

/// The [TI-83 Plus user guide](https://archive.org/details/ti-83-plus-guidebook/)
/// (on page 47) has the following to say about order of operations:
///
/// > EOS evaluates the functions in an expression in this order:
/// >
/// > 1. Functions that precede the argument, such as √(, sin(, or log(
/// > 2. Functions that are entered after the argument, such as ², ⁻¹, !, ° and conversions
/// > 3. Powers and roots, such as 2^5 or 5√32
/// > 4. Permutations (nPr) and combinations (nCr)
/// > 5. Multiplication, implied multiplication, and division
/// > 6. Addition and subtraction
/// > 7. Relational functions, such as > or <
/// > 8. Logic operator and
/// > 9. Logic operators or and xor
/// >
/// > Within a priority level, EOS evaluates function from left to right.
/// > Calculations within parentheses are evaluated first.
///
/// It also has misleading statements about the priority of negation:
///
/// > negation is in the third level of the EOS hierarchy. Functions in the first level,
/// > such as squaring, are evaluated before negation.
///
/// According to the previous list, squaring is at level 2 and by experiment exponentiation
/// evaluates before negation which would mean negation actually sits between levels
/// 3 and 4.
fn expression<'a, I>() -> impl Parser<'a, I, Expression, extra::Err<Rich<'a, Token>>> + Clone
where
    I: ValueInput<'a, Token = Token, Span = SimpleSpan>,
{
    use chumsky::pratt::{infix, left, prefix};

    recursive(|term| {
        // Parenthesized expressions are either regular parens, or an unary operator
        // that wraps its single parameter: a function, basically.
        let open_paren = (just(0x10).to(None).labelled("("))
            .or(choice((just(0xbc).to(UnaryOperator::Sqrt).labelled("√("),)).map(Some));
        let parenthesized = open_paren
            .then(term)
            .then_ignore(just(0x11).or_not().labelled(")"))
            .map(|(op, term): (Option<UnaryOperator>, Expression)| match op {
                Some(op) => (op, term).into(),
                None => term,
            });

        let atom = choice((
            parenthesized,
            variable().or(ans()).map(Expression::Variable),
            number().map(|x| Expression::Literal(x.into())),
            string().map(Expression::Literal),
        ));

        let negate = just(0xb0).labelled("negative sign");

        atom.pratt((
            // Exponentiation
            postfix(
                9,
                choice((
                    just(0x0d).labelled("²").to(2),
                    just(0x0f).labelled("³").to(3),
                )),
                |expr, power: i32| (BinaryOperator::Exponentiate, expr, Value::from(power)).into(),
            ),
            // Negation
            prefix(7, negate, |x: Expression| {
                Expression::UnaryOp(UnaryOperator::Negate, x.into())
            }),
            // Multiplication and division
            infix(
                left(5),
                choice((
                    just(0x82).to(BinaryOperator::Multiply).labelled("*"),
                    just(0x83).to(BinaryOperator::Divide).labelled("/"),
                    // Implicit multiplication forbids negation in the right-hand term,
                    // possibly because that's easily confused with subtraction.
                    negate
                        .not()
                        .rewind()
                        .map_err(|e: Rich<'a, Token>| {
                            Rich::custom(
                                *e.span(),
                                "The second multiplicand may not begin with a \
                                 negative sign in implicit multiplication. \
                                 (Did you mean to subtract?)",
                            )
                        })
                        .to(BinaryOperator::Multiply),
                )),
                |l, op, r| (op, l, r).into(),
            ),
            // Addition and subtraction
            infix(
                left(4),
                choice((
                    just(0x70).to(BinaryOperator::Add).labelled("+"),
                    just(0x71).to(BinaryOperator::Subtract).labelled("-"),
                )),
                |l, op, r| (op, l, r).into(),
            ),
            // Relational operators
            infix(
                left(3),
                choice((
                    just(0x6a).to(BinaryOperator::Equal).labelled("="),
                    just(0x6b).to(BinaryOperator::LessThan).labelled("<"),
                    just(0x6c).to(BinaryOperator::GreaterThan).labelled(">"),
                    just(0x6d).to(BinaryOperator::LessThanOrEqual).labelled("≤"),
                    just(0x6e)
                        .to(BinaryOperator::GreaterThanOrEqual)
                        .labelled("≥"),
                    just(0x6f).to(BinaryOperator::NotEqual).labelled("≠"),
                )),
                |l, op, r| (op, l, r).into(),
            ),
        ))
    })
}

#[test]
fn parens() {
    assert_eq!(
        expression()
            .parse_tokens(tokenize!("1+(2-3)"))
            .into_result(),
        Ok((
            BinaryOperator::Add,
            Value::from(1),
            Expression::from((BinaryOperator::Subtract, Value::from(2), Value::from(3),)),
        )
            .into())
    )
}

#[test]
fn excess_parens() {
    assert_eq!(
        expression()
            .parse_tokens(tokenize!("((3)-(1))"))
            .into_result(),
        Ok((BinaryOperator::Subtract, Value::from(3), Value::from(1)).into()),
    )
}

#[test]
fn add_sub_neg() {
    use crate::value::Value;
    use ast::Expression;

    assert_eq!(
        expression().parse_tokens(tokenize!("1+~2-4")).into_result(),
        Ok((
            BinaryOperator::Subtract,
            Expression::from((
                BinaryOperator::Add,
                Value::from(1),
                Expression::from((UnaryOperator::Negate, Value::from(2)))
            )),
            Value::from(4),
        )
            .into())
    )
}

#[test]
fn multiply_divide() {
    assert_eq!(
        expression().parse_tokens(tokenize!("1/2")).into_result(),
        Ok((BinaryOperator::Divide, Value::from(1), Value::from(2)).into()),
    )
}

#[test]
fn implicit_multiplication() {
    use ast::Reference;
    use var::NumVar;

    assert_eq!(
        expression().parse_tokens(tokenize!("AB+4")).into_result(),
        Ok(Expression::from((
            BinaryOperator::Add,
            Expression::from((
                BinaryOperator::Multiply,
                Reference::from(NumVar::A),
                Reference::from(NumVar::B),
            )),
            Value::from(4)
        )))
    );
}

/// Implicit multiplication does not occur if a value is followed directly by
/// a negative sign: it's an error.
#[test]
fn implicit_multiplication_not_negation() {
    assert_eq!(
        expression().parse_tokens(tokenize!("3~2")).into_result(),
        Err(vec![Rich::custom(
            SimpleSpan::new(1, 2),
            "The second multiplicand may not begin with a negative sign \
             in implicit multiplication. (Did you mean to subtract?)"
        )]),
    );
}

#[test]
fn basic_strings() {
    // With closing quote
    assert_eq!(
        string().parse_tokens(tokenize!(r#""FOO""#)).into_result(),
        Ok(Value::string_from_bytes(tokenize!("FOO")).unwrap())
    );
    // Closed by sto
    assert_eq!(
        string()
            .then_ignore(just(0x04))
            .parse_tokens(tokenize!(r#""HI->"#))
            .into_result(),
        Ok(Value::string_from_bytes(tokenize!("HI")).unwrap())
    );
    // Closed by end of line
    assert_eq!(
        string()
            .then_ignore(just(0x3f))
            .parse_tokens(tokenize!("\"TRUNK\n"))
            .into_result(),
        Ok(Value::string_from_bytes(tokenize!("TRUNK")).unwrap())
    );
}

#[test]
fn string_lines() {
    assert_eq!(
        lines()
            .parse_tokens(tokenize!("\"FIRST:LINE\n\"SECOND\""))
            .into_result(),
        Ok(vec![
            ast::Node::Expression(
                Value::string_from_bytes(tokenize!("FIRST:LINE"))
                    .unwrap()
                    .into()
            ),
            ast::Node::Expression(
                Value::string_from_bytes(tokenize!("SECOND"))
                    .unwrap()
                    .into()
            )
        ])
    );
}

#[test]
fn control_flow_lines() {
    assert_eq!(
        lines()
            .parse_tokens(tokenize!("If 1:Then:Else:End"))
            .into_result(),
        Ok(vec![
            ast::Node::If(Value::from(1).into()),
            ast::Node::Then,
            ast::Node::Else,
            ast::Node::End,
        ])
    );
}
