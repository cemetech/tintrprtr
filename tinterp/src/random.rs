use crate::Float;

/// Generates random numbers with the same algorithm as TI.
///
/// TI's BASIC implementation uses [L'Ecuyer's algorithm](https://doi.org/10.1145/62959.62969) to
/// generate random numbers. Various individuals have shared reimplementations:
///
///  * <https://stackoverflow.com/a/32790991/2658436>
///  * <https://github.com/TI-Toolkit/awesome-ti-docs/blob/588f22d59d2fb793263ce64a5192ecfc39b0804e/rand.py>
///
/// The behavior of these in case of overflow in intermediate operations is not well-specified, so
/// this implementation may misbehave if seeded with very large values. (By experiment, `1e99->rand`
/// yields 0.2423427314 for the next value on a real calculator.)
pub struct RandomNumberGenerator {
    seed1: i32,
    seed2: i32,
}

impl Default for RandomNumberGenerator {
    fn default() -> Self {
        RandomNumberGenerator {
            seed1: 12345,
            seed2: 67890,
        }
    }
}

impl RandomNumberGenerator {
    const MOD1: i32 = 2147483563;
    const MOD2: i32 = 2147483399;
    const MULT1: i32 = 40014;
    const MULT2: i32 = 40692;

    pub fn set_seed(&mut self, mut seed: Float) {
        seed = seed.truncate().abs();
        if seed.is_zero() {
            *self = Default::default();
        } else {
            self.seed1 = ((seed * Self::MULT1).unwrap() % Self::MOD1)
                .expect("integer mod integer should always succeed")
                .to_real_as::<_>()
                .unwrap();
            self.seed2 = ((seed % Self::MOD2).unwrap() % Self::MOD2)
                .expect("integer mod integer should always succeed")
                .to_real_as::<_>()
                .unwrap();
        }
    }

    /// Generate a random number in the range [0,1).
    pub fn next(&mut self) -> Float {
        self.seed1 = (self.seed1 * Self::MULT1) % Self::MOD1;
        self.seed2 = (self.seed2 * Self::MULT2) % Self::MOD2;

        let result = (Float::from(self.seed1 - self.seed2) / Self::MOD1).unwrap();
        if result < Float::zero() {
            (result + 1).unwrap()
        } else {
            result
        }
    }
}
