/// Alias for a single TI-BASIC token.
///
/// Tokens are either one or two bytes, where the presence of a second byte is
/// known by the first byte having a specific value which indicates it is a
/// two-byte token.
///
/// In this representation, a token is interpreted as a big-endian value where
/// the high byte is zero if the token has only one byte, and nonzero
/// otherwise.
///
/// For example, the → token (one byte, 0x04) is represented as 0x0004 and the
/// "a" token (two bytes, 0xbb, 0xb0) is 0xbbb0.
pub type Token = u16;

/// Converts a collection of bytes to a collection of tokens.
pub struct BytesToTokens<B, S>
where
    B: AsRef<[u8]>,
    S: std::borrow::Borrow<titokens::DetokenizerSpec>,
{
    detokenizer: titokens::Detokenizer<S>,
    buffer: B,
    offset: usize,
}

impl<B> BytesToTokens<B, &'static titokens::DetokenizerSpec>
where
    B: AsRef<[u8]>,
{
    pub fn new(bytes: B) -> Self {
        BytesToTokens {
            detokenizer: titokens::Detokenizer::ti83plus(titokens::DetokenizeMode::Display),
            buffer: bytes,
            offset: 0,
        }
    }
}

/// Iterates over Token values in the given buffer.
///
/// If the buffer contains any invalid tokens, `next` will panic when the
/// invalid bytes are encountered.
impl<B, S> Iterator for BytesToTokens<B, S>
where
    B: AsRef<[u8]>,
    S: std::borrow::Borrow<titokens::DetokenizerSpec>,
{
    type Item = Token;

    fn next(&mut self) -> Option<Token> {
        if self.offset >= self.buffer.as_ref().len() {
            None
        } else {
            let buf = &self.buffer.as_ref()[self.offset..];
            if let Some((n_bytes, _s)) = self.detokenizer.detokenize_one(buf) {
                self.offset += n_bytes;

                Some(u16::from_be_bytes(if n_bytes > 1 {
                    [buf[0], buf[1]]
                } else {
                    [0, buf[0]]
                }))
            } else {
                panic!(
                    "Invalid token bytes found at input offset {}: {:?}",
                    self.offset,
                    &buf[..2]
                );
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let max = self.buffer.as_ref().len() - self.offset;
        let min = max / 2;
        (min, Some(max))
    }
}
