use derive_more::From;

use crate::token::BytesToTokens;
use crate::{EvalError, EvalResult, Float};
use titokens::{DetokenizeMode, Detokenizer};

#[derive(Debug, PartialEq, Clone, From)]
pub enum Value {
    /// A floating-point number, real or complex
    Number(Float),
    /// A string of zero or more tokens
    String(Vec<u16>),
    // List, ...
}

impl Value {
    pub fn eq(&self, other: &Value) -> Result<Value, EvalError> {
        use Value::*;
        match (self, other) {
            (Number(l), Number(r)) => Ok(Value::from(l == r)),
            (String(l), String(r)) => Ok(Value::from(l == r)),
            (l, r) => Err(EvalError::DataType),
        }
    }

    pub fn cmp(&self, other: &Value) -> Result<std::cmp::Ordering, EvalError> {
        use Value::*;

        match (self, other) {
            (Number(l), Number(r)) => l.partial_cmp(r).ok_or(EvalError::DataType),
            // Numbers can only be compared with other numbers
            (_, Number(_)) | (Number(_), _) |
            // Strings can only be compared for equality
            (_, String(_)) | (String(_), _) => Err(EvalError::DataType),
        }
    }

    pub fn as_bool(&self) -> Result<bool, EvalError> {
        match self {
            Value::Number(n) => Ok(!n.is_zero()),
            _ => Err(EvalError::DataType),
        }
    }

    /// Construct a string from raw bytes.
    ///
    /// Returns an error if the provided bytes cannot fit into a string variable.
    pub fn string_from_bytes<B: AsRef<[u8]>>(bytes: B) -> EvalResult {
        Self::string_from_tokens(BytesToTokens::new(bytes.as_ref()).collect::<Vec<_>>())
    }

    /// Construct a string from tokens.
    ///
    /// Returns an error if the tokens would be larger than the allowed size of
    /// a string, once encoded to bytes.
    pub fn string_from_tokens<B: Into<Vec<crate::token::Token>>>(tokens: B) -> EvalResult {
        let t = tokens.into();
        let bytes_len = t.iter().fold(0usize, |sum, &t| {
            let sz = if (t & 0xff00) == 0 { 1 } else { 2 };
            sum + sz
        });
        if bytes_len + 2 > u16::MAX as usize {
            Err(EvalError::Memory)
        } else {
            Ok(Value::String(t))
        }
    }

    pub fn pow<O: Into<Float>>(&self, exponent: O) -> Result<Value, EvalError> {
        match self {
            Value::Number(num) => Ok(num.pow(exponent.into())?.into()),
            Value::String(_) => Err(EvalError::DataType),
        }
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Value::Number(num) => write!(f, "{}", num),
            Value::String(tokens) => {
                let mut detokenizer = Detokenizer::ti83plus(DetokenizeMode::Display);
                for (i, &token) in tokens.iter().enumerate() {
                    if let Some(str) = detokenizer.detokenize_u16(token) {
                        write!(f, "{}", str)?;
                    } else {
                        panic!("Token {:#x} is unknown (at string index {})", token, i);
                    }
                }
                Ok(())
            }
        }
    }
}

impl From<bool> for Value {
    fn from(b: bool) -> Self {
        let n = if b {
            Float::real(1).unwrap()
        } else {
            Float::zero()
        };
        Value::Number(n)
    }
}

impl From<i32> for Value {
    fn from(f: i32) -> Self {
        Value::Number(Float::from(f))
    }
}

impl std::ops::Add for Value {
    type Output = EvalResult;

    fn add(self, rhs: Self) -> EvalResult {
        match (self, rhs) {
            (Value::Number(l), Value::Number(r)) => Ok(Value::Number((l + r)?)),
            (Value::String(mut l), Value::String(r)) => {
                l.extend(r);
                Ok(Value::String(l))
            }
            (Value::Number(_), _) | (Value::String(_), _) => Err(EvalError::DataType),
        }
    }
}

impl std::ops::Sub for Value {
    type Output = EvalResult;

    fn sub(self, rhs: Self) -> EvalResult {
        self + (-rhs)?
    }
}

impl std::ops::Neg for Value {
    type Output = EvalResult;

    fn neg(self) -> EvalResult {
        match self {
            Value::Number(num) => Ok(Value::Number(-num)),
            Value::String(_) => Err(EvalError::DataType),
        }
    }
}

impl std::ops::Mul for Value {
    type Output = EvalResult;

    fn mul(self, rhs: Self) -> EvalResult {
        match (self, rhs) {
            (Value::Number(l), Value::Number(r)) => Ok(Value::Number((l * r)?)),
            (Value::Number(_), _) | (Value::String(_), _) => Err(EvalError::DataType),
        }
    }
}

impl std::ops::Div for Value {
    type Output = EvalResult;

    fn div(self, rhs: Self) -> EvalResult {
        match (self, rhs) {
            (Value::Number(l), Value::Number(r)) => Ok(Value::Number((l / r)?)),
            (Value::Number(_), _) | (Value::String(_), _) => Err(EvalError::DataType),
        }
    }
}

impl std::ops::Not for Value {
    type Output = EvalResult;

    fn not(self) -> EvalResult {
        if let Value::Number(num) = self {
            // 0 becomes 1, anything else becomes 0
            Ok(num.is_zero().into())
        } else {
            Err(EvalError::DataType)
        }
    }
}
