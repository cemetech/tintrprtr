use derive_more::From;
use num_enum::{IntoPrimitive, TryFromPrimitive};

/// A numeric variable, named with a single letter.
#[derive(Debug, PartialEq, Eq, Clone, Copy, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum NumVar {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Theta,
}

/// A string variable, Str1 through Str0 (and sometimes more).
///
/// There are only meant to be 10 available strings (`Str1` through `Str0`),
/// but strings are named with a two-byte token consisting of a string marker
/// and a second byte that is the string index, where EOS accepts any string
/// index from 0 to 255.
///
/// When converting a string from an integer, the number in the token is
/// one more than the value: `StringVar::from(0)` is `Str1`. This is the same
/// interpretation as the second byte of a string token.
#[derive(Debug, PartialEq, Eq, Clone, Copy, From)]
pub struct StringVar(u8);

impl StringVar {
    pub fn index(&self) -> usize {
        self.0 as usize
    }
}
