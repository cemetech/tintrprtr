mod util;

use tintrprtr::ast::Reference;
use tintrprtr::value::Value;
use tintrprtr::var::NumVar;
use titokens_macros::tokenize;
use util::*;

#[test]
fn single_line_if() {
    let ex = execute(tokenize!("If 1:1"));
    assert_eq!(
        ex.vars
            .get(Reference::Ans)
            .expect("Ans should have a value"),
        &Value::Number(1.into()),
    );

    let ex = execute(tokenize!("If 0:A"));
    assert_eq!(
        ex.vars.get(Reference::Ans),
        None,
        "Branch should not be taken, so Ans should remain unset \
         and reference to undefined variable is permitted"
    );
}

#[test]
fn if_then() {
    // Both contained lines are executed in the taken block
    let ex = execute(tokenize!(
        "If 100:Then\n\
         0->A:1->B\n\
         End"
    ));
    assert_eq!(
        ex.vars.get(NumVar::A).expect("A should have a value"),
        &Value::Number(0.into())
    );
    assert_eq!(
        ex.vars.get(NumVar::B).expect("B should also have a value"),
        &Value::Number(1.into())
    );

    // None of the contained lines are executed in an untaken block
    let ex = execute(tokenize!("If 0:Then:\"UNTAKEN->Str1\n7:End"));
    assert_eq!(ex.vars.get(Reference::Ans), None,);
}

#[test]
fn if_then_else() {
    let ex = execute(tokenize!("If 0:Then:0:Else:1:End"));
    assert_eq!(
        ex.vars.get(Reference::Ans).expect("Ans should be set"),
        &Value::Number(1.into()),
    );

    let ex = execute(tokenize!("If 1:Then:1:Else:0:End"));
    assert_eq!(
        ex.vars.get(Reference::Ans).expect("Ans should be set"),
        &Value::Number(1.into()),
    );
}

#[test]
fn if_if_then() {
    // Entire inner If/Then is skipped so X/Y refs aren't errors,
    // but the line after End is no longer inside the outer If.
    let ex = execute(tokenize!(
        "If 0\n\
         If 1:Then\n\
         X:Y:End:42"
    ));
    assert_eq!(
        ex.vars.get(Reference::Ans).expect("Ans should be set"),
        &Value::Number(42.into()),
    );
}

#[test]
fn countdown_goto() {
    let vars = execute(tokenize!(
        "0->A"
        "Lbl N"
        "A+1->A"
        "If A<4"
        "Goto N"
    ))
    .vars;

    assert_eq!(
        vars.get(Reference::from(NumVar::A))
            .expect("A should have a value"),
        &Value::Number(4.into())
    );
}

#[test]
fn countdown_while() {
    let vars = execute(tokenize!(
        "While A<4"
        "A+1->A"
        "End"
    ))
    .vars;

    assert_eq!(
        vars.get(Reference::from(NumVar::A))
            .expect("A should have a value"),
        &Value::Number(4.into())
    );
}

/// Closing a loop with an End inside a bare If is allowed.
///
/// This is a bad behavior of EOS's parser that we want to support.
#[test]
fn if_end_misuse() {
    let ex = execute(tokenize!(
        "While 1"
        "X+1->X"
        "If X<2"
        "End"
    ));

    assert_eq!(
        ex.vars
            .get(Reference::from(NumVar::X))
            .expect("X should have a value"),
        &Value::Number(2.into())
    );
}

#[test]
fn goto_out_of_loop() {
    let ex = execute(tokenize!(
        "While X=0"
         "If X=0"
         "Goto A"   // 1: jump out of loop
        "End"
        "Lbl A"
        "1->X"
        "End"       // 2: evaluate loop condition again
        "3->X"      // 3: loop condition is now false
    ));

    assert_eq!(
        ex.vars.get(Reference::from(NumVar::X)).unwrap(),
        &Value::Number(3.into())
    );
}
