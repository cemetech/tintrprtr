use io8x::display::{Display, Headless};
use tintrprtr::ast::{Program, Reference};
use tintrprtr::Executor;
use titokens_macros::tokenize;

mod util;

#[test]
fn display_nothing() {
    let mut ex = Executor::<_, Headless>::new(
        Program::from_token_bytes(tokenize!("Disp ")).expect("Program should parse correctly"),
    );
    // Display is full of junk
    for px in ex.display.buf_mut().iter_mut() {
        *px = 1;
    }

    ex.step().expect("Should execute successfully");
    assert!(ex.is_terminated());
    assert_eq!(ex.vars.get(Reference::Ans), None);
    assert_eq!(ex.homescreen.x(), 0);
    assert_eq!(ex.homescreen.y(), 0);

    // Homescreen was drawn to the display (it's now blank)
    assert!(
        ex.display.is_blank(),
        "Homescreen should be drawn to display, but is instead\n{:?}",
        ex.display.buf()
    );
}
