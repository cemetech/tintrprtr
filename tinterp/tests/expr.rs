use std::str::FromStr;
use tintrprtr::ast::Reference;
use tintrprtr::value::Value;
use tintrprtr::var::{NumVar, StringVar};
use tintrprtr::Float;
use titokens_macros::tokenize;

use util::{execute, execute_with_vars};

mod util;

#[test]
fn value_of_numeric_var() {
    let ex = execute_with_vars(tokenize!("X"), &[(NumVar::X.into(), 123i32.into())]);

    assert_eq!(ex.vars.get(Reference::Ans), Some(&123i32.into()),)
}

#[test]
fn pythagorean_thereom() {
    let vars = execute_with_vars(
        tokenize!("√(A²+B²→C"),
        &[
            (NumVar::A.into(), 3i32.into()),
            (NumVar::B.into(), 4i32.into()),
        ],
    )
    .vars;
    assert_eq!(vars.get(NumVar::C), Some(&5i32.into()));
}

#[test]
fn comparison_operators() {
    assert_eq!(execute(tokenize!("1=1")).into_ans(), Some(true.into()));
    assert_eq!(execute(tokenize!("2=1")).into_ans(), Some(false.into()));
    assert_eq!(execute(tokenize!("1!=1")).into_ans(), Some(false.into()));
    assert_eq!(execute(tokenize!("2!=1")).into_ans(), Some(true.into()));
}

#[test]
fn implicit_multiplication_vars() {
    let a_is_3 = &[(NumVar::A.into(), 2i32.into())][..];

    assert_eq!(
        execute_with_vars(tokenize!("3A"), a_is_3).into_ans(),
        Some(6i32.into())
    );

    // Swapping the order is also fine
    assert_eq!(
        execute_with_vars(tokenize!("A3"), a_is_3).into_ans(),
        Some(6i32.into())
    );
}

/// Implicit multiplication occurs if a value is followed by any parenthesized expression.
#[test]
fn implicit_multiplication_parens() {
    assert_eq!(execute(tokenize!("4(2")).into_ans(), Some(8i32.into()),);

    assert_eq!(
        execute(tokenize!("(~2-1)7")).into_ans(),
        Some((-21i32).into()),
    );
}

#[test]
fn implicit_multiplication_fns() {
    assert_eq!(
        execute(tokenize!("2sqrt(2")).into_ans(),
        Some(Float::from_str("2.82842712474619").unwrap().into())
    )
}

#[test]
fn implicit_multiplication_precedence() {
    // Cubing binds more closely to the second 2 than the first, so this is 2*8 rather
    // than 4^3.
    assert_eq!(execute(tokenize!("2(2)^^3")).into_ans(), Some(16.into()));

    // Low-precedence operations don't bind that closely, though. 4+3, not 2*5.
    assert_eq!(execute(tokenize!("2(2)+3")).into_ans(), Some(7.into()));
}

#[test]
fn string_storage() {
    let vars = execute(tokenize!("\"HELLO->Str1\n\"World\"")).vars;

    assert_eq!(
        vars.get(StringVar::from(0)),
        Some(&Value::string_from_bytes(tokenize!("HELLO")).unwrap())
    );
    assert_eq!(
        vars.get(Reference::Ans),
        Some(&Value::string_from_bytes(tokenize!("World")).unwrap())
    );
}
