/*
mod util;

use tintrprtr::parser::var::NumVar;
use tintrprtr::value::Value;
use titokens_macros::tokenize;
use util::execute;

/// Storing a string to a numeric variable is allowed but doesn't do anything.
#[test]
fn string_to_number_ignored() {
    let ex = execute(tokenize!("\"A->X"));
    assert_eq!(ex.vars.get(NumVar::X), None);
    assert_eq!(
        ex.into_ans(),
        Some(Value::string_from_bytes(b"A".as_slice()).unwrap())
    );
}
*/
