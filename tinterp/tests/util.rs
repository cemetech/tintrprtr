// This module contains helper functions that aren't used by all tests but is included
// individually in each test, which causes unused warnings for any item that isn't used
// in any given test.
#![allow(unused)]

use tintrprtr::ast::{self, Reference};
use tintrprtr::display::Headless;
use tintrprtr::error::EvalError;
use tintrprtr::value::Value;
use tintrprtr::Executor;

pub type Program = Vec<ast::Node>;

pub fn execute(tokens: &[u8]) -> Executor<Program, Headless> {
    execute_with_vars(tokens, &[])
}

pub fn execute_error(tokens: &[u8]) -> Result<Executor<Program, Headless>, EvalError> {
    execute_error_with_vars(tokens, &[])
}

pub fn execute_with_vars(
    tokens: &[u8],
    vars: &[(Reference, Value)],
) -> Executor<Program, Headless> {
    execute_error_with_vars(tokens, vars).expect("An error occurred while executing")
}

pub fn execute_error_with_vars<'a>(
    tokens: &'a [u8],
    vars: &[(Reference, Value)],
) -> Result<Executor<Program, Headless>, EvalError> {
    let program = tintrprtr::parse_program(tokens).expect("failed to parse program");
    let mut exec = Executor::<_, Headless>::new(program);
    for (var, val) in vars {
        exec.vars.set(*var, val.clone()).unwrap();
    }

    while !exec.is_terminated() {
        exec.step()?;
    }

    Ok(exec)
}
