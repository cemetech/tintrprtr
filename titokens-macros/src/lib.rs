//! Macros for compile-time (de)tokenization.

use litrs::StringLit;
use proc_macro::{Delimiter, Group, Ident, Literal, Punct, Spacing, Span, TokenStream, TokenTree};
use titokens::Tokenizer;

fn error(msg: &str) -> TokenStream {
    [
        TokenTree::Ident(Ident::new("compile_error", Span::call_site())),
        TokenTree::Punct(Punct::new('!', Spacing::Alone)),
        TokenTree::Group(Group::new(
            Delimiter::Parenthesis,
            TokenTree::Literal(Literal::string(msg)).into(),
        )),
    ]
    .into_iter()
    .collect()
}

/// Tokenize literal strings at compile-time.
///
/// Expands to a literal bytestring of tokens. If the provided string(s) cannot be
/// tokenized, it is a compile error. If multiple strings are given, they are
/// concatenated as separate lines (as if "\n" were inserted between them).
///
/// ```
/// # use titokens_macros::tokenize;
/// assert_eq!(
///     tokenize!("Disp \"HELLO!"),
///     &[0xde, 0x2a, b'H', b'E', b'L', b'L', b'O', 0x2d]
/// );
///
/// assert_eq!(
///     tokenize!(
///         "1->A"
///         "Disp A"
///     ),
///     b"1\x04A\x3f\xdeA"
/// );
/// ```
#[proc_macro]
pub fn tokenize(input: TokenStream) -> TokenStream {
    let tokenizer = Tokenizer::ti83plus();
    let mut tokens = vec![];

    let mut args = input.into_iter().peekable();
    while let Some(arg) = args.next() {
        if let TokenTree::Literal(lit) = arg {
            let value = StringLit::try_from(lit).unwrap();
            // TODO subspan(start..) on s for output if better-spans and position info
            // is available in the error.
            if let Err(e) = tokenizer.tokenize(value.value().as_bytes(), &mut tokens) {
                return error(&e.to_string());
            }
        } else {
            return error("all arguments to tokenize! must be string literals");
        }

        if args.peek().is_some() {
            tokenizer
                .tokenize(b"\n", &mut tokens)
                .expect("Newline should tokenize okay");
        }
    }

    TokenTree::Literal(Literal::byte_string(&tokens)).into()
}
