use titokens_macros::tokenize;

#[test]
fn no_args() {
    assert_eq!(tokenize!(), b"",)
}

#[test]
fn one_arg() {
    assert_eq!(tokenize!("HELLO"), b"HELLO",);
}

#[test]
fn multiple_args() {
    assert_eq!(
        tokenize!(
            "HELLO "
            "WORLD"
        ),
        b"HELLO\x29\x3fWORLD",
    );
}
