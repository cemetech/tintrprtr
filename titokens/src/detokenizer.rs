use radix_trie::{Trie, TrieCommon};
use std::borrow::Borrow;
use std::collections::{HashMap, VecDeque};
use std::io::Read;
use std::sync::LazyLock;

use super::{TokenKind, Tokenizer, XmlParseResult, TI83PLUS_TOKENS_XML};

/// Describes the tokens recognized by a [`Detokenizer`].
///
/// This state is separate from the detokenizer itself because the detokenizer has other
/// state. By holding a reference to a spec, the cost of building a spec can be amortized
/// across many detokenizers.
#[derive(Debug, Clone)]
pub struct DetokenizerSpec {
    // TODO if strings allocate in an arena, every String here could be a ref to an allocation in
    // the arena, saving a lot of allocations. (say, &bumpalo::collections::String)
    trie: Trie<Vec<u8>, String>,
    /// Maps tokens to the set of tokens that they are a suffix of.
    ///
    /// For instance, " " is a suffix of both "Goto " and " or ", so " " maps to
    /// ["Goto ", " or "] here.
    suffix_map: HashMap<String, Vec<String>>,
    /// Number of Unicode characters in the longest string corresponding to any single token.
    max_token_chars: usize,
    /// Maximum number of bytes making up any single token.
    max_token_bytes: usize,
}

impl DetokenizerSpec {
    /// Construct a detokenizer spec from Tokens.xml specification.
    ///
    /// Behaves similarly to [`Tokenizer::from_tokens_xml`] but constructs a detokenizer.
    pub fn from_tokens_xml<R: Read>(r: R) -> XmlParseResult<Self> {
        // the detokenizer is the same as a tokenizer but swapped keys and values in the trie,
        // so build a tokenizer and then make a reversed copy; this is simpler to write than doing
        // the XML parsing again, though a little less efficient
        let tokenizer = Tokenizer::from_tokens_xml(r)?;

        let mut trie = Trie::<Vec<u8>, String>::new();
        let mut max_token_chars = 0usize;
        let mut max_token_bytes = 0usize;
        for (bytes, str, kind) in tokenizer.iter_tokens() {
            if kind != TokenKind::Canonical {
                // Detokenization always returns canonical strings; ignore others
                continue;
            }

            max_token_chars = std::cmp::max(max_token_chars, str.chars().count());
            max_token_bytes = std::cmp::max(max_token_bytes, bytes.len());
            trie.insert(bytes.into(), str.into());
        }

        let mut suffix_map = HashMap::<String, Vec<String>>::new();
        // Check each token (including non-canonical ones) to find the others that are
        // suffixes of them. We must include non-canonical tokens because a match on aliases
        // can still cause an incorrect round-trip.
        for (_, x, _) in tokenizer.iter_tokens() {
            for (_, y, _) in tokenizer.iter_tokens() {
                if x == y {
                    // Artifact of getting the cartesian product; ignore
                    continue;
                }
                if y.ends_with(x) {
                    if let Some(v) = suffix_map.get_mut(x) {
                        v.push(y.to_owned());
                    } else {
                        suffix_map.insert(x.into(), vec![y.into()]);
                    }
                }
            }
        }

        Ok(DetokenizerSpec {
            trie,
            suffix_map,
            max_token_chars,
            max_token_bytes,
        })
    }

    /// Get a spec for TI-83+ tokens.
    ///
    /// This is equivalent to calling [`from_tokens_xml`](Self::from_tokens_xml) with
    /// [`TI83PLUS_TOKENS_XML`], but cannot return errors because the built-in definition
    /// is known to be valid.
    pub fn ti83plus() -> &'static DetokenizerSpec {
        static CACHED: LazyLock<DetokenizerSpec> = LazyLock::new(|| {
            DetokenizerSpec::from_tokens_xml(TI83PLUS_TOKENS_XML)
                .expect("built-in tokens should be valid")
        });

        &*CACHED
    }
}

/// The output format to use when detokenizing.
///
/// Non-`Display` modes inserts breaks when output may be ambiguous; see [`Detokenizer`]
/// documentation for details.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum DetokenizeMode {
    /// Insert backslashes as breaks.
    ///
    /// This mode is compatible with most tools and is appropriate for general use.
    Source,
    /// Insert zero-width non-joiners (U+200C) as breaks.
    ///
    /// ZWNJs are generally invisible in displayed text, which is convenient if the resulting text
    /// is intended to be used primarily for humans to read while still ensuring correct
    /// re-tokenization behavior if the text is copied while preserving the invisible characters.
    ///
    /// Although nicer for humans to read, the correctness of output in this format depends on
    /// all intermediate systems supporting Unicode correctly. In addition, few tools understand
    /// the use of ZWNJ in this context and may ignore them or throw errors when encountered.
    Invisible,
    /// Do not insert breaks.
    ///
    /// This should be used only when it is expected that the output will never be fed back into
    /// a tokenizer, or if it is known that the output will behave correctly if re-tokenized.
    Display,
}

#[derive(thiserror::Error, Debug, PartialEq)]
#[error("Mode string did not correspond to any known mode")]
pub struct ModeFromStrError;

/// Case-insensitive conversion from variant names as string.
impl std::str::FromStr for DetokenizeMode {
    type Err = ModeFromStrError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let variants = [
            ("source", DetokenizeMode::Source),
            ("invisible", DetokenizeMode::Invisible),
            ("display", DetokenizeMode::Display),
        ];
        debug_assert!(variants.iter().all(|(s, _)| s.is_ascii()));

        for (s, value) in variants {
            if s.eq_ignore_ascii_case(input) {
                return Ok(value);
            }
        }
        Err(ModeFromStrError)
    }
}

impl DetokenizeMode {
    fn escape_sequence(&self) -> Option<&'static str> {
        match self {
            DetokenizeMode::Display => None,
            DetokenizeMode::Source => Some("\\"),
            DetokenizeMode::Invisible => Some("\u{200c}"),
        }
    }
}

/// Translates tokens into human-readable text.
///
/// ### Disambiguation
///
/// When detokenizing, it's possible for the output to contain strings
/// corresponding to tokens that should not be transformed to those tokens.
/// For instance, the sequence of tokens [p, i] detokenizes to the string "pi"
/// but "pi" may also be an alternate spelling of the token π. If the simple
/// detokenization is used, retokenizing the output would not be a lossless
/// operation.
///
/// When detokenizing in a non-[`Display`](DetokenizeMode::Display) mode, breaks
/// will be inserted in order to ensure retokenization is lossless: the tokens
/// [p, i] would be detokenized to the string "p\i" to explicitly mark that
/// it is two tokens and should not be interpreted as π.
#[derive(Debug, Clone)]
pub struct Detokenizer<Spec> {
    spec: Spec,
    mode: DetokenizeMode,
    /// Buffers outputs to determine when strings must be split when in Source mode.
    last_outputs: RingBuffer<char>,
}

/// Errors that may occur while detokenizing tokens.
#[derive(Debug, thiserror::Error)]
#[error("Byte sequence {0:?} does not correspond to any known tokens")]
pub struct Error<'a>(&'a [u8]);

impl Detokenizer<&'static DetokenizerSpec> {
    /// Get a detokenizer based on [`DetokenizerSpec::ti83plus`].
    pub fn ti83plus(mode: DetokenizeMode) -> Self {
        Detokenizer::new(DetokenizerSpec::ti83plus(), mode)
    }
}

impl<Spec> Detokenizer<Spec>
where
    Spec: Borrow<DetokenizerSpec>,
{
    /// Construct a detokenizer
    pub fn new(spec: Spec, mode: DetokenizeMode) -> Self {
        let buffer_capacity = if mode.escape_sequence().is_some() {
            spec.borrow().max_token_chars
        } else {
            0
        };

        Detokenizer {
            spec,
            mode,
            last_outputs: RingBuffer::with_capacity(buffer_capacity),
        }
    }

    pub fn spec(&self) -> &DetokenizerSpec {
        self.spec.borrow()
    }

    pub fn detokenize_u16(&mut self, value: u16) -> Option<&str> {
        let bytes = value.to_be_bytes();
        let token_bytes = if bytes[0] == 0 {
            &bytes[1..]
        } else {
            &bytes[..]
        };

        match self.detokenize_one(token_bytes) {
            None => None,
            Some((n, s)) => {
                assert_eq!(
                    n,
                    token_bytes.len(),
                    "integer token {:#06x} should be {} bytes, but detokenized to {}",
                    value,
                    token_bytes.len(),
                    n
                );
                Some(s)
            }
        }
    }

    /// Detokenize the first token from the given data.
    ///
    /// If there is no valid token at the beginning of the data, returns None. Otherwise the
    /// contained tuple is the size of the token in bytes and its canonical string representation.
    pub fn detokenize_one(&mut self, tokens: &[u8]) -> Option<(usize, &str)> {
        let spec = self.spec.borrow();
        match spec.trie.get_ancestor(tokens) {
            None => None,
            Some(t) => {
                let bytes = t.key().unwrap();
                let string = t.value().unwrap();

                if let Some(escape_sequence) = self.mode.escape_sequence() {
                    // If the concatenation of the output buffer and the current token string ends with
                    // any member of the suffix map for this token, insert a backslash to disambiguate.
                    let is_suffix_match = |suffix_candidate: &String| {
                        // all() is true if empty, but no buffer means no need for disambiguation
                        if self.last_outputs.is_empty() {
                            return false;
                        }
                        assert!(!suffix_candidate.is_empty());

                        let buffer_chars = self.last_outputs.0.iter().copied();
                        let output_chars = buffer_chars.chain(string.chars()).rev();
                        let suffix_chars = suffix_candidate.chars().rev();

                        suffix_chars.zip(output_chars).all(|(s, o)| s == o)
                    };

                    let mut suffix_candidates = spec.suffix_map.get(string).into_iter().flatten();
                    let needs_escape = suffix_candidates.any(is_suffix_match);

                    if needs_escape {
                        self.last_outputs.clear();
                        return Some((0, escape_sequence));
                    }
                }

                for c in string.chars() {
                    self.last_outputs.push(c);
                }
                Some((bytes.len(), string))
            }
        }
    }

    /// Detokenize the entire provided data
    ///
    /// Returns the concatenation of the canonical string representations of each token in sequence,
    /// or an error if an unrecognized sequence appears anywhere.
    pub fn detokenize<'tok>(&mut self, mut tokens: &'tok [u8]) -> Result<String, Error<'tok>> {
        let mut out = String::new();
        while !tokens.is_empty() {
            match self.detokenize_one(tokens) {
                Some((len, str)) => {
                    out.push_str(str);
                    tokens = &tokens[len..];
                }
                None => {
                    return Err(Error(
                        &tokens[..std::cmp::min(tokens.len(), self.spec().max_token_bytes)],
                    ))
                }
            }
        }

        Ok(out)
    }
}

/// Simple wrapper for a VecDeque that avoids growing beyond the initial capacity.
#[derive(Debug, Clone)]
struct RingBuffer<T>(VecDeque<T>)
where
    T: Clone;

impl<T> RingBuffer<T>
where
    T: Clone,
{
    fn with_capacity(capacity: usize) -> Self {
        RingBuffer(VecDeque::with_capacity(capacity))
    }

    fn push(&mut self, value: T) {
        if self.0.capacity() == 0 {
            // A buffer with zero capacity cannot contain any values.
            return;
        }

        if self.0.len() == self.0.capacity() {
            self.pop();
        }
        self.0.push_back(value);
    }

    fn pop(&mut self) -> Option<T> {
        self.0.pop_front()
    }

    fn clear(&mut self) {
        while self.pop().is_some() {}
    }

    fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

/// Token aliases are considered when splitting.
#[test]
fn splits_on_aliases() {
    let xml = r#"
    <Tokens>
        <Token byte="$00" string="ꙮ">
            <Alt string="😇👁" /> 
        </Token>
        <Token byte="$01" string="😇" />
        <Token byte="$02" string="👁" />
    </Tokens>
    "#;

    let spec = DetokenizerSpec::from_tokens_xml(xml.as_bytes()).unwrap();
    let mut detokenizer = Detokenizer::new(spec, DetokenizeMode::Invisible);
    assert_eq!(
        detokenizer.detokenize(&[1, 2]).unwrap(),
        "😇\u{200c}👁",
        "Characters should be split because they could match the alternate of token 0"
    );
}

/// In Source mode, ambiguous token sequences are explicitly split.
#[test]
fn pi_sto_split_source() {
    let mut detokenizer = Detokenizer::ti83plus(DetokenizeMode::Source);

    assert_eq!(
        detokenizer.detokenize(b"\xac\xbb\xc0\xbb\xb8").unwrap(),
        "πp\\i"
    );
    assert_eq!(detokenizer.detokenize(b"\x04\x71\x6c").unwrap(), "→-\\>");
}

/// Ensure that Display mode actually works.
///
/// Display mode would always panic in an earlier version; this prevents regressions.
#[test]
fn display_mode_works() {
    let mut detokenizer = Detokenizer::ti83plus(DetokenizeMode::Display);

    assert_eq!(detokenizer.detokenize(b"\xbb\xc0\xbb\xb8").unwrap(), "pi");
}
