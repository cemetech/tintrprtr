//! Conversions between textual and binary (tokenized) representations of tokens.
//!
//! While it's usually most convenient for humans to write BASIC programs as plain
//! text, calculators only understand them as compact tokens of one or two bytes
//! each. Tokenized data on computers is usually stored in calculator program files
//! that wrap tokenized data in a small amount of meta-information, allowing the
//! tokenized nature of the data to be easily noted.

pub mod detokenizer;
pub mod tokenizer;

pub use detokenizer::{DetokenizeMode, Detokenizer, DetokenizerSpec};
pub use tokenizer::Tokenizer;

/// Errors that can be encountered while construction a (de)tokenizer from XML.
// TODO use xml::EventReader::position() to report nicer error locations for XML
// TODO use miette to print XML errors nicely?
#[derive(thiserror::Error, Debug)]
pub enum XmlParseError {
    /// A `<Token>` element does not have a mandatory `byte` attribute.
    #[error("Token element lacks a byte attribute")]
    TokenMissingByte,
    /// The `byte` attribute of a `<Token>` element is not parseable as a hex number like `$ff`.
    ///
    /// The contained value is the string attribute of the token.
    #[error("Token byte value {0:?} is not a recognized hex byte value")]
    TokenByteMalformed(String),
    /// An `<Alt>` element does not have a mandatory `string` attribute.
    #[error("Alt element lacks a string attribute")]
    AltMissingString,
    #[error("String {0:?} was specified for both byte sequence {1:x?} and {2:x?}")]
    DuplicateString(String, Vec<u8>, Vec<u8>),
    /// The `string` attribute of a `<Token>` or `<Alt>` element contains a character that is not
    /// permitted to appear in token strings.
    ///
    /// The contained values are the bytes specified for the token and the character that was
    /// found but is not permitted.
    #[error("String value for token bytes {0:?} contains ignored character {1:?}")]
    IgnoredChar(Vec<u8>, char),
    /// The provided XML was unparseable.
    #[error("Unable to parse tokens XML")]
    Xml(#[from] xml::reader::Error),
}

pub type XmlParseResult<T> = Result<T, XmlParseError>;

/// Whether a token definition is considered primary or alternate.
///
/// Alternate token definitions are accepted when tokenizing strings, but detokenizing
/// always emits the canonical string for a token.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TokenKind {
    Canonical,
    Alternate,
}

/// The built-in XML data describing TI-83+ tokens used by [`Tokenizer::ti83plus`] and [`Detokenizer::ti83plus`].
pub static TI83PLUS_TOKENS_XML: &[u8] = include_bytes!("../Tokens.xml");

/// Ignored characters, allowed in source but never present in tokens.
const SOURCE_IGNORED_CHARS: &[char] = &['\r', '\t'];
