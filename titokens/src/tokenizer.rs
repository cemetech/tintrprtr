use radix_trie::{Trie, TrieCommon};
use std::collections::HashMap;
use std::io::{Read, Write};
use std::sync::LazyLock;
use xml::attribute::OwnedAttribute;
use xml::reader::XmlEvent;

use super::{TokenKind, XmlParseError, XmlParseResult, SOURCE_IGNORED_CHARS, TI83PLUS_TOKENS_XML};

/// Errors that may occur while tokenizing text.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// A portion of input text cannot be represented with tokens.
    ///
    /// The contained string is a segment of the input beginning at the location where
    /// no matching tokens exist.
    #[error("String {0:?} cannot be represented as tokens")]
    Untokenizable(String),
    /// The provided bytes are not valid UTF-8.
    #[error("Provided source code was not valid UTF-8")]
    InvalidUtf8(#[from] std::str::Utf8Error),
    /// An I/O error occurred while writing to tokens output.
    #[error("I/O error writing tokens to output")]
    Io(#[from] std::io::Error),
}

/// Translates text into token sequences.
pub struct Tokenizer {
    // TODO: keys and values are always small, can mess with types
    // to avoid a lot of allocations.
    trie: Trie<String, (Vec<u8>, TokenKind)>,
}

impl Tokenizer {
    /// Convert the provided text into tokens.
    ///
    /// Backslashes can be inserted into the provided source to force particular interpretations
    /// of characters where they may otherwise be ambiguous. For instance, `pi` could be a
    /// two-token sequence of `p` and `i`, or it may be an alternate spelling of the `π` token.
    /// By writing the source as `p\i` instead, interpretation would be forced to the two-token
    /// version. A double backslash (`\\`) is treated as a single backslash.
    ///
    /// ```
    /// # use titokens::Tokenizer;
    /// let source = "I LIKE pi";
    /// let mut tokens = vec![];
    /// Tokenizer::ti83plus().tokenize(source.as_bytes(), &mut tokens)
    ///     .expect("source could not be tokenized");
    /// assert_eq!(tokens, b"I\x29LIKE\x29\xac");
    /// ```
    pub fn tokenize<W: Write>(&self, source: &[u8], mut out: W) -> Result<W, Error> {
        let mut source = std::str::from_utf8(source)?;
        let mut preceded_backslash = false;

        while !source.is_empty() {
            match source.chars().next() {
                Some(c) if SOURCE_IGNORED_CHARS.contains(&c) => {
                    source = &source[1..];
                    continue;
                }
                // TODO also support ZWNJ as in DetokenizeMode::Invisible
                Some('\\') => {
                    // Ignore a leading backslash unless its predecessor was also a backslash.
                    // This allows backslashes to split tokens and not otherwise be interpreted,
                    // but a double backslash is treated as a backslash token.
                    if !preceded_backslash {
                        source = &source[1..];
                        preceded_backslash = true;
                        continue;
                    }
                    preceded_backslash = false;
                }
                Some(_) => { /* Permitted character in a token */ }
                None => unreachable!(),
            }

            // Find the longest prefix of the source that matches a token: that's the token we want
            let matched_token = match self.trie.get_ancestor(source) {
                None => {
                    // No prefix of the input is a valid token. Capture enough chars to represent
                    // any token and return that in the error.
                    let max_tok_chars = self
                        .trie
                        .keys()
                        .fold(0usize, |a, k| std::cmp::max(a, k.chars().count()));
                    return Err(Error::Untokenizable(
                        source.chars().take(max_tok_chars).collect(),
                    ));
                }
                Some(t) => t,
            };
            assert!(
                !matched_token.prefix().is_empty(),
                "root should not have a value, but got {:?}",
                matched_token.value()
            );

            let consumed_bytes = matched_token.key().unwrap().len();
            source = &source[consumed_bytes..];

            let (token, _canonical) = matched_token.value().unwrap();
            out.write_all(token)?;
        }

        Ok(out)
    }

    /// Get an iterator over all tokens recognized by a tokenizer.
    ///
    /// ```no_run
    /// # use titokens::Tokenizer;
    /// for (bytes, string, _kind) in Tokenizer::ti83plus().iter_tokens() {
    ///     println!("{:?} => {:?}", string, bytes);
    /// }
    /// ```
    pub fn iter_tokens(&self) -> Tokens {
        Tokens(self.trie.iter())
    }

    /// Construct a tokenizer from a Tokens.xml specification.
    ///
    /// Returns an error if the provided XML is malformed, otherwise a tokenizer.
    ///
    /// ```
    /// # use titokens::Tokenizer;
    /// let minimal_xml = br#"
    ///   <Tokens>
    ///     <Token byte="$00" string="Hello, world!"/>
    ///   </Tokens>
    /// "#;
    /// Tokenizer::from_tokens_xml(&minimal_xml[..]).expect("XML was not valid");
    /// ```
    pub fn from_tokens_xml<R: Read>(r: R) -> XmlParseResult<Self> {
        let mut reader = xml::EventReader::new(r);
        // The overall mapping from plaintext string to byte sequence
        let mut trie = Trie::<String, (Vec<u8>, TokenKind)>::new();
        // The sequence of bytes representing the current token
        let mut bytes: Vec<u8> = vec![];

        loop {
            match reader.next()? {
                XmlEvent::StartElement {
                    name, attributes, ..
                } => {
                    let mut attributes = Self::collect_attrs(&attributes);

                    let mut insert = |s, bytes, kind| {
                        match trie.get_mut(&s) {
                            None => trie.insert(s, (bytes, kind)),
                            Some((collision, _)) => {
                                return Err(XmlParseError::DuplicateString(
                                    s,
                                    std::mem::replace(collision, vec![]),
                                    bytes,
                                ));
                            }
                        };
                        Ok(())
                    };

                    match name.local_name.as_str() {
                        "Token" => {
                            let (byte, string) = Self::interpret_token(attributes)?;

                            bytes.push(byte);
                            if let Some(s) = string {
                                // Ignored chars must not appear in tokens, otherwise the token
                                // could never be emitted.
                                for &c in SOURCE_IGNORED_CHARS {
                                    if s.contains(c) {
                                        return Err(XmlParseError::IgnoredChar(bytes, c));
                                    }
                                }
                                // Do not capture the empty string: tokenizing should
                                // never consume empty strings as tokens, otherwise
                                // every valid program has an infinite set of possible
                                // tokenizations.
                                if !s.is_empty() {
                                    insert(s, bytes.clone(), TokenKind::Canonical)?;
                                }
                            }
                        }
                        "Alt" => {
                            // Alias of the current bytes. Acts like a token with no byte value,
                            // taking bytes from context.
                            match attributes.remove("string") {
                                Some(s) => {
                                    insert(s, bytes.clone(), TokenKind::Alternate)?;
                                }
                                None => return Err(XmlParseError::AltMissingString),
                            }
                        }
                        _ => { /* Ignore unrecognized elements */ }
                    }
                }

                XmlEvent::EndElement { name } => {
                    match name.local_name.as_str() {
                        "Token" => {
                            bytes.pop();
                        }
                        "Alt" => { /* Nothing to do */ }
                        _ => { /* Ignore unrecognized elements */ }
                    }
                }
                XmlEvent::EndDocument => break,
                XmlEvent::StartDocument { .. }
                | XmlEvent::ProcessingInstruction { .. }
                | XmlEvent::CData(..)
                | XmlEvent::Comment(..)
                | XmlEvent::Characters(..)
                | XmlEvent::Whitespace(..) => { /* Unused */ }
            }
        }

        // TODO enforce that the same string cannot be represented by multiple tokens

        Ok(Tokenizer { trie })
    }

    /// Collect the handled attributes from the given list into a map.
    fn collect_attrs(attributes: &[OwnedAttribute]) -> HashMap<String, String> {
        attributes
            .iter()
            .map(|attr| (attr.name.local_name.clone(), attr.value.clone()))
            .collect::<HashMap<_, _>>()
    }

    fn interpret_token(
        mut attributes: HashMap<String, String>,
    ) -> XmlParseResult<(u8, Option<String>)> {
        let byte = match attributes.remove("byte") {
            // tokens must have a byte value
            None => return Err(XmlParseError::TokenMissingByte),
            // byte value is a two-digit hex number with '$' prefix
            Some(s) if !s.starts_with('$') => return Err(XmlParseError::TokenByteMalformed(s)),
            Some(s) => match u8::from_str_radix(&s[1..], 16) {
                Ok(x) => x,
                Err(_) => return Err(XmlParseError::TokenByteMalformed(s)),
            },
        };
        let string = attributes.remove("string").map(|s| {
            // Replace backslash-escaped newlines with actual newlines
            if s.contains("\\n") {
                s.replace("\\n", "\n")
            } else {
                s
            }
        });

        Ok((byte, string))
    }

    /// Get a tokenizer for TI-83+ tokens.
    ///
    /// This is equivalent to calling [`from_tokens_xml`](Self::from_tokens_xml) with
    /// [`TI83PLUS_TOKENS_XML`], but cannot return errors because the built-in definition
    /// is known to be valid.
    pub fn ti83plus() -> &'static Self {
        static CACHED: LazyLock<Tokenizer> = LazyLock::new(|| {
            Tokenizer::from_tokens_xml(TI83PLUS_TOKENS_XML)
                .expect("Built-in Tokens.xml was invalid")
        });

        &*CACHED
    }
}

/// An iterator over all tokens known by a tokenizer.
pub struct Tokens<'a>(radix_trie::iter::Iter<'a, String, (Vec<u8>, TokenKind)>);

/// Iterates over pairs of token string and bytes making up the token.
impl<'a> Iterator for Tokens<'a> {
    type Item = (&'a [u8], &'a str, TokenKind);

    fn next(&mut self) -> Option<Self::Item> {
        self.0
            .next()
            .map(|(k, &(ref v, t)): (&String, &(Vec<u8>, _))| (&**v, &**k, t))
    }
}

#[cfg(test)]
fn test_tokenize(bytes: &[u8]) -> Result<Vec<u8>, Error> {
    use std::io::Cursor;

    let mut buf = vec![];
    Tokenizer::ti83plus().tokenize(bytes, Cursor::new(&mut buf))?;
    Ok(buf)
}

#[test]
fn empty_source_is_valid() {
    assert_eq!(test_tokenize(b"").unwrap(), b"");
}

#[test]
fn hello_world() {
    assert_eq!(
        test_tokenize("Disp \"Hello, world!\"".as_bytes()).unwrap(),
        &[
            0xde, 0x2a, // Disp "
            0x48, 0xbb, 0xb4, 0xbb, 0xbc, 0xbb, 0xbc, 0xbb, 0xbf, 0x2b, 0x29, // Hello,
            0xbb, 0xc7, 0xbb, 0xbf, 0xbb, 0xc2, 0xbb, 0xbc, 0xbb, 0xb3, // world
            0x2d, 0x2a // !"
        ]
    );
}

#[test]
fn errors_on_illegal_chars() {
    match test_tokenize("ꙮ".as_bytes()) {
        Err(Error::Untokenizable(s)) => assert_eq!(s, "\u{a66e}"),
        r => panic!("Expected Untokenizable error, but got {:?}", r),
    }
}

#[test]
fn can_escape() {
    assert_eq!(
        test_tokenize(r"pi≠p\i".as_bytes()).unwrap(),
        &[0xac, 0x6f, 0xbb, 0xc0, 0xbb, 0xb8],
        "Backslash should split pi token into p and i"
    );

    assert_eq!(
        test_tokenize(br"\\").unwrap(),
        &[0xbb, 0xd7],
        "Double backslash should become a backslash token"
    );
}

#[test]
fn escaped_token_strings_are_interpreted() {
    let simple_xml = br#"<Tokens><Token byte="$3F" string="\n"/></Tokens>"#;
    let tokenizer = Tokenizer::from_tokens_xml(simple_xml.as_slice()).unwrap();

    assert_eq!(
        tokenizer.trie.get("\n").unwrap().0,
        &[0x3f],
        "\\n should be interpreted as a newline in XML"
    );

    let mut tokens = vec![];
    tokenizer.tokenize(b"\n\n", &mut tokens).unwrap();
    assert_eq!(
        &tokens,
        &[0x3f, 0x3f],
        "newlines should tokenize to the newline token"
    );
}

#[test]
fn forbids_duplicate_strings() {
    let xml = r#"
      <Tokens>
        <Token string="asdf" byte="$00" />
        <Token string="jkl;" byte="$01">
          <Alt string="asdf" />
        </Token>
      </Tokens>
    "#;
    assert!(
        Tokenizer::from_tokens_xml(xml.as_bytes()).is_err(),
        "Duplicate string asdf should cause from_tokens_xml to return an XML error"
    );
}
